# README #

This is my Summer Bursary project (2014 & 2015): 
There are two versions of this project, the latest is in the folder "Security_App_Sharing" and the other is in the folder "Security_App_System_File".

* "Security_App_Sharing" extends Dropbox by allowing users to upload files securely (please note this is a prototype and therefore Google Drive is being treated as a "secure" server). Files that are uploaded securely are stored in the client's Google Drive account and the fileID's to access those files are stored under their Google ID in a MySQL database with the path that that file should be presented in the Dropbox library. Users can also specify which email addresses they would like to share a "secure" file with. Secure shared files are recorded in the MySQL database under the user's email address that has been authorised to access the file. The permission the user has been granted and the fileID and path it should be displayed in the Dropbox library is also stored in the MySQL server for shared files. However access of the shared files is incomplete as I have not yet discovered an easy API method for this. To run this App please refer to "Security_App_Sharing" under "Run me".

* "Security_App_System_File" extends Dropbox by allowing users to upload files securely (please note this is a prototype and therefore Google Drive is being treated as a "secure" server). Files that are uploaded securely are stored in the client's Google Drive account and the fileID's to access those files are encrypted (with symmetric-key encryption) and stored in a ".App_System_File" under the path the file should be presented at in the Dropbox library. The symmetric-key is stored in the properties of a ".App_System_File" in the user's Google Drive. When a "secure" file is being accessed the ".App_System_File"s are consulted and the fileID for the file at the path is decrypted so the file can be retrieved. To run this App please refer to "Security_App_System_File" under "Run me".

For more information about my project, please see the videos in the folder "Information_Videos"

#Files:#
The files supplied are a git repository. Subsequently git was installed for manipulation of this repository and optionally SourceTree (GUI).

#Run me#

##Security_App_Sharing##

Please note: "/Security_App_Sharing/RunMe.mp4" is a video which walks through the set up.

###Requirements###
* Vagrant must be installed (https://www.vagrantup.com/downloads.html)
* Virtual box must be installed (https://www.virtualbox.org/wiki/Downloads)

###Vagrant###
When 'vagrant up' is run, the "Security_App_Sharing/Vagrantfile" sets up a precise64 (64 bit) box. The "Security_App_Sharing/Vagrantfile" also runs the "Security_App_Sharing/bootstrap.sh" script which installs emacs, Apache2, MySQL (username: root, password: root), PHP and PHP-curl. The "Security_App_Sharing/bootstrap.sh" also replaces the auto-generated "php.ini" file (/etc/php5/apache2/php.ini) with the "Security_App_Sharing/php.ini" file which subsequently changes the fields "error_reporting", "display_errors" & "display_startup_errors".

###Steps:###
1. Firstly download the "Requirements" above and the files from this repository.
2. Create (or use an existing) Dropbox (www.dropbox.com/) and Google account (www.google.co.nz). 
3. From the Dropbox App Console (www.dropbox.com/developers/apps) create an app. Select the option to make this App a "Dropbox API app" and specify that the app stores "Files and datastores". Select the option "no" so that the app is not limited to its own folder and specify that the app needs access to "All file types". Next give the app a name and select "Create app". In the new app's settings, add "http://127.0.0.1:4567/src/Secure_Dropbox/web-file-browser.php/dropbox-auth-finish" as one of the "OAuth2 Redirect URIs". Finally, copy the "App key" and "App secret" supplied (in the new app's settings) to "/Security_App_Sharing/src/Secure_Dropbox/web-file-browser.app" (supplied by this repository) so that your file looks like:

		{

    		"key": "<App_key>",

    		"secret": "<App_secret>"

		}
		
4. From the Google Developers Console (https://code.google.com/apis/console/) create a new project and assign it a name. Turn on "Drive API" in "APIs" found under "APIs & auth". Next "Create new Client ID" in "Credentials" found under "APIs & auth". Specify it as a "Web application" and choose your "EMAIL ADDRESS". Also assign a "PRODUCT NAME" before saving. Next specify the "AUTHORIZED REDIRECT URIS" as "http://127.0.0.1:4567/src/Secure_Dropbox/Authorize_Google.php" and then "Create Client ID". Finally, copy/assign the "CLIENT ID" and "CLIENT SECRET" to the corresponding variables ("$client_id" and "$client_secret") at the beginning of the file: "/Security_App_Sharing/src/Secure_Dropbox/Authorize_Google.php" (supplied by this repository). Make sure you save this change.
5. Run an Apache2 server: cd into the "/Security_App_Sharing" directory (supplied by this repository) which has the "Vagrantfile" in it. Run the command "vagrant up" which will create the virtual box (with precise64). This command will also make installs as specified in the section above called "Vagrant". Finally this command will run an Apache2 server at "127.0.0.1:4567".
6. In your web browser visit the URL: "http://127.0.0.1:4567/src/Secure_Dropbox/web-file-browser.php/".
7. Follow the on-screen prompts to link your accounts to this app.

##Security_App_System_File##

Please note: "/Security_App_System_File/RunMe.mp4" is a video which walks through the set up.

###Requirements###
* Vagrant must be installed (https://www.vagrantup.com/downloads.html)
* Virtual box must be installed (https://www.virtualbox.org/wiki/Downloads)

###Vagrant###
When 'vagrant up' is run, the "Security_App_System_File/Vagrantfile" sets up a precise64 (64 bit) box. The "Security_App_System_File/Vagrantfile" also runs the "Security_App_System_File/bootstrap.sh" script which installs emacs, Apache2, mcrypt, PHP and PHP-curl. The "Security_App_System_File/bootstrap.sh" also replaces the auto-generated "php.ini" file (/etc/php5/apache2/php.ini) with the "Security_App_System_File/php.ini" file which subsequently changes the fields "error_reporting", "display_errors" & "display_startup_errors".

###Steps:###
1. Firstly download the "Requirements" above and the files from this repository.
2. Create (or use an existing) Dropbox (www.dropbox.com/) and Google account (www.google.co.nz). 
3. From the Dropbox App Console (www.dropbox.com/developers/apps) create an app. Select the option to make this App a "Dropbox API app" and specify that the app stores "Files and datastores". Select the option "no" so that the app is not limited to its own folder and specify that the app needs access to "All file types". Next give the app a name and select "Create app". In the new app's settings, add "http://127.0.0.1:4567/src/Secure_Dropbox/web-file-browser.php/dropbox-auth-finish" as one of the "OAuth2 Redirect URIs". Finally, copy the "App key" and "App secret" supplied (in the new app's settings) to "/Security_App_System_File/src/Secure_Dropbox/web-file-browser.app" (supplied by this repository) so that your file looks like:

		{

    		"key": "<App_key>",

    		"secret": "<App_secret>"

		}
		
4. From the Google Developers Console (https://code.google.com/apis/console/) create a new project and assign it a name. Turn on "Drive API" in "APIs" found under "APIs & auth". Next "Create new Client ID" in "Credentials" found under "APIs & auth". Specify it as a "Web application" and choose your "EMAIL ADDRESS". Also assign a "PRODUCT NAME" before saving. Next specify the "AUTHORIZED REDIRECT URIS" as "http://127.0.0.1:4567/src/Secure_Dropbox/Authorize_Google.php" and then "Create Client ID". Finally, copy/assign the "CLIENT ID" and "CLIENT SECRET" to the corresponding variables ("$client_id" and "$client_secret") at the beginning of the file: "/Security_App_System_File/src/Secure_Dropbox/Authorize_Google.php" (supplied by this repository). Make sure you save this change.
5. Run an Apache2 server: cd into the "/Security_App_System_File" directory (supplied by this repository) which has the "Vagrantfile" in it. Run the command "vagrant up" which will create the virtual box (with precise64). This command will also make installs as specified in the section above called "Vagrant". Finally this command will run an Apache2 server at "127.0.0.1:4567".
6. In your web browser visit the URL: "http://127.0.0.1:4567/src/Secure_Dropbox/web-file-browser.php/".
7. Follow the on-screen prompts to link your accounts to this app.


##/Dropbox_examples/authorize.php##
This PHP file must be run from the command prompt.

###Requirements###
* Vagrant must be installed (https://www.vagrantup.com/downloads.html)
* Virtual box must be installed (https://www.virtualbox.org/wiki/Downloads)
* PHP must be installed so that php files can be run via the command line

###Steps:###
1. Firstly download the "Requirements" above and the files from this repository.
2. Create (or use an existing) Dropbox account (www.dropbox.com/). 
3. From the Dropbox App Console (www.dropbox.com/developers/apps) create an app. Select the option to make this App a "Dropbox API app" and specify that the app stores "Files and datastores". Select the option "no" so that the app is not limited to its own folder and specify that the app needs access to "All file types". Next give the app a name and select "Create app". Finally, copy the "App key" and "App secret" supplied (in the new app's settings) to "/Security_App_Sharing/src/Dropbox_examples/app-info.app" (or .json) (supplied by this repository) so that your file looks like:

		{

    		"key": "<App_key>",

    		"secret": "<App_secret>"

		}
	
4. In the "/Security_App_Sharing/src/Dropbox_examples" directory run "authorize.php" in the command prompt with the following command: 'php authorize.php app-info.app access-token.auth' or 'php authorize.php app-info.json access-token.auth' (based on step 3). Follow the on screen prompts until the program has finished.
This program will have approved the App on the client's account (logged in), by reading the App's details from "app-info.app" (or "app-info.json") and returning an access token which should be stored so that this approval method does not have to be repeated each time. Subsequently this access token is stored in "access-token.auth" (in the current directory).

##/Dropbox_examples/account-info.php##
This .php file must be run on the command prompt by:

1. Follow the above "/Dropbox_examples/authorize.php" run me to create an "access-token.auth" file.

2. In the command prompt run "php account-info.php access-token.auth"

##/Dropbox_examples/web-file-browser.php##
This .php file must be run using a server:

###Requirements###
* Vagrant must be installed (https://www.vagrantup.com/downloads.html)
* Virtual box must be installed (https://www.virtualbox.org/wiki/Downloads)

###Steps:###
1. Firstly download the "Requirements" above and the files from this repository.
2. Create (or use an existing) Dropbox account (www.dropbox.com/). 
3. From the Dropbox App Console (www.dropbox.com/developers/apps) create an app. Select the option to make this App a "Dropbox API app" and specify that the app stores "Files and datastores". Select the option "no" so that the app is not limited to its own folder and specify that the app needs access to "All file types". Next give the app a name and select "Create app". In the new app's settings, add "http://127.0.0.1:4567/src/Dropbox_examples/web-file-browser.php/dropbox-auth-finish" as one of the "OAuth2 Redirect URIs". Finally, copy the "App key" and "App secret" supplied (in the new app's settings) to "/Security_App_Sharing/src/Dropbox_examples/web-file-browser.app" (supplied by this repository) so that your file looks like:

		{

    		"key": "<App_key>",

    		"secret": "<App_secret>"

		}
		
4. Run an Apache2 server: cd into the "/Security_App_Sharing" directory (supplied by this repository) which has the "Vagrantfile" in it. Run the command "vagrant up" which will create the virtual box (with precise64). This command will also make installs as specified in the section above called "Vagrant". Finally this command will run an Apache2 server at "127.0.0.1:4567".

5. Type the path to the web-file-browser.php file into your browser:

    http://127.0.0.1:4567/src/Dropbox_examples/web-file-browser.php

    Note: if "Error(400)" occurs, just copy the "Invalid redirect_uri" link provided under "More details for developers" and paste it into the "OAuth 2" "Redirect URIs" section of your app (at https://www.dropbox.com/developers/apps/), and add it as described in step 3. Then repeat from step 5.

6. Follow any on screen prompts.

##Dropbox SDK requirements (handled automatically by the vagrant script: "bootstrap.sh")##
* PHP 5.3+ and subsequently PHP 5.3.10 is being downloaded (at the moment) when "vagrant up" is run. Check which version has been downloaded with the command "php -version" when you have ssh'ed into the precise64 vagrant environment.
* PHP with 64 bit integers and subsequently vagrant runs precise64 (64 bit).
* PHP with curl extension, and subsequently the "bootstrap.sh" script installs this on booting the vagrant precise64 environment.
