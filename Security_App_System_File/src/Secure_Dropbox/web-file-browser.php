<?php

require_once __DIR__.'/../../lib/Dropbox/strict.php';

$appInfoFile = __DIR__."/web-file-browser.app";

// NOTE: You should be using Composer's global autoloader.  But just so these examples
// work for people who don't have Composer, we'll use the library's "autoload.php".
require_once __DIR__.'/../../lib/Dropbox/autoload.php';

require_once __DIR__.'/App_System_File_Handler.php';	//added
require_once __DIR__.'/Handle_Secure_Server.php';		//added

use \Dropbox as dbx;

$requestPath = init();

/**
 * Displays a file browser allowing the user to manipulate the client's Dropbox in the web browser. 
 * This also includes uploading files "securely" by storing them in the client's
 * Google Drive library and the file ID to access that file is stored under the system file (in Dropbox)
 * at the path: '/.App_System_File'. Please note: "secure" is used in the comments as this is a 
 * prototype, as we are pretending Google Drive is a secure server.
 * 
 * Adapted from dropbox-sdk-php-1.1.4/examples/web-file-browser.php
 * 
 * @author Craig Atkinson
 * @date 05/12/2014
 */
session_start();

/*
 * If the request path is "/", it renders a file or folder in the client's Dropbox library. If the file or folder specified by 
 * the http path cannot be found in the Dropbox library it is proccessed based on the information in the system file.
 */
if ($requestPath === "/") {
    $dbxClient = getClient();
    
    //check that the user is logged into Dropbox
    if ($dbxClient === false) {
        header("Location: ".getPath("dropbox-auth-start"));
        exit;
    }

    //check that the user is logged into the "Secure" Server   
    $hss = new Handle_Secure_Server();
    if($hss->isAccessTokenExpired()){
    	unset($_SESSION['access_token']);
		header("Location: " . $hss->getAuthURL());
    }

    $path = "/";
    if (isset($_GET['path'])) $path = $_GET['path'];
	//create folder
	if(isset($_GET['folderName']) && isset($_GET['submit']) && $_GET['submit'] == "create" && $_GET['folderName'] != ""){
		
		if(isValidFolderName($_GET['folderName'], $path)){
			getClient()->createFolder(($path === "/"? "":$path)."/".$_GET['folderName']);
			header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
		}else{
			echo "Error, folder could not be created as its name was either too long<br>
					and/or included illegal characters:<br>
					<pre>/ \ < > : \" | ? * .</pre>";
			echoReturnLink($path);
			echo "<br>".($path === "/"? "":$path)."/".$_GET['folderName'];
		}
		
    }
    //delete folder
    else if(isset($_GET['folderName']) && isset($_GET['submit']) && $_GET['submit'] == "delete" && $_GET['folderName'] != ""){
		$folderPath = ($path === "/"? "":$path)."/".$_GET['folderName'];
    	try{
    		if(!isFilePath($folderPath)){
    			getClient()->delete($folderPath);
    			removeSecureFilesInFolder($folderPath);
				header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
	    	}
    	}catch(dbx\Exception_BadResponseCode $e){
    		if(removeSecureFilesInFolder($folderPath))
				header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
    	}
    	echo "Error: Could not find folder to delete.";
		echoReturnLink($path);
    }
    //browse
    else{

		$entry = $dbxClient->getMetadataWithChildren($path);

		if($entry == null){
			if (!isFilePath($path)){
    	    	echo renderFolder($path, null);
	    	}
    		else {
	    		echoReturnToParentLink($path);
        		echo renderSecureFile($path);
	    	}
    	}else{
	    	if ($entry['is_dir']) {
    	    	echo renderFolder($entry['path'], $entry['contents']);
	    	}
    		else {
				echoReturnToParentLink($path);
        		echo renderNormalFile($entry);
	    	}
    	}
    }
}

/*
 * Deletes the file specified by the path URL parameter 
 */
else if ($requestPath == "/delete") {
	$path = $_GET['path'];
	echo "Deleted: ".$path;
	echoReturnToParentLink($path);
	$dbxClient = getClient();
	$entry = $dbxClient->getMetadataWithChildren($path);

	if($entry == null){
		$sysFileHandler = new App_System_File_Handler($dbxClient, '/.App_System_File');
		$hss = new Handle_Secure_Server();
		$hss->delete($sysFileHandler->get($path));
		$dbxClient = getClient();
		$sysFileHandler->remove($path);
	}else{
		$dbxClient->delete($path);
	}
}

/*
 * Downloads/displays the Dropbox file specified by the path URL parameter
 */
else if ($requestPath == "/download") {
    $dbxClient = getClient();

    if ($dbxClient === false) {
        header("Location: ".getPath("dropbox-auth-start"));
        exit;
    }

    if (!isset($_GET['path'])) {
        header("Location: ".getPath(""));
        exit;
    }
    $path = $_GET['path'];

    $fd = tmpfile();
    $metadata = $dbxClient->getFile($path, $fd);

    header("Content-Type: $metadata[mime_type]");
    fseek($fd, 0);
    fpassthru($fd);
    fclose($fd);
}

/*
 * The file specified by the user is uploaded to either Dropbox or the "secure" Google Drive library 
 * (assocating the file ID with the file's path (in Dropbox), stored in the system file: '/.App_System_File'). The file is saved to the "secure"
 * library if the "$_POST['secure']" is set. Files are not saved if they have the same name as a file in the same directory on the Dropbox library
 * or in the "secure" library.
 */
else if ($requestPath === "/upload") {
    if (empty($_FILES['file']['name'])) {
        echo renderHtmlPage("Error", "Please choose a file to upload");
        echoReturnToParentLink("/");
        exit;
    }

    if (!empty($_FILES['file']['error'])) {
        echo renderHtmlPage("Error", "Error ".$_FILES['file']['error']." uploading file.  See <a href='http://php.net/manual/en/features.file-upload.errors.php'>the docs</a> for details");
        echoReturnToParentLink("/");
        exit;
    }

	$dbxClient = getClient();
   	$sysFileHandler = new App_System_File_Handler($dbxClient, '/.App_System_File');
	$remoteDir = "/";
	if (isset($_POST['folder'])) 
		$remoteDir = $_POST['folder'];
	$remotePath = rtrim($remoteDir, "/")."/".$_FILES['file']['name'];
	
	$dbxMetadata = $dbxClient->getMetadataWithChildren($remotePath);
	
	if($sysFileHandler->containsKey($remotePath) || $dbxMetadata != null){
		fileNameTakenError($remotePath);		
	}else{
		if(!isset($_POST['secure'])){
	    	$fp = fopen($_FILES['file']['tmp_name'], "rb");
	    	$result = $dbxClient->uploadFile($remotePath, dbx\WriteMode::add(), $fp);
   			fclose($fp);
	    	$str = print_r($result, true);
    	    echoReturnToParentLink($remotePath);
    		echo renderHtmlPage("Uploading File", "Result: <pre>$str</pre>");
    	}else{
			$hss = new Handle_Secure_Server();
			$encrypted_fileID = $hss->uploadFile();
    		$sysFileHandler->add($remotePath, $encrypted_fileID);
    		echoReturnToParentLink($remotePath);
    		echo renderHtmlPage("Uploaded File", $remotePath);
    	}
    }
}

/*
 * Starts the Dropbox authentication process
 */
else if ($requestPath === "/dropbox-auth-start") {
    $authorizeUrl = getWebAuth()->start();
    header("Location: $authorizeUrl");
}

/*
 * Finishes the Dropbox authentication process, handling any errors
 */
else if ($requestPath === "/dropbox-auth-finish") {
    try {
        list($accessToken, $userId, $urlState) = getWebAuth()->finish($_GET);
        // We didn't pass in $urlState to finish, and we're assuming the session can't be
        // tampered with, so this should be null.
        assert($urlState === null);
    }
    catch (dbx\WebAuthException_BadRequest $ex) {
        respondWithError(400, "Bad Request");
        // Write full details to server error log.
        // IMPORTANT: Never show the $ex->getMessage() string to the user -- it could contain
        // sensitive information.
        error_log("/dropbox-auth-finish: bad request: " . $ex->getMessage());
        exit;
    }
    catch (dbx\WebAuthException_BadState $ex) {
        // Auth session expired.  Restart the auth process.
        header("Location: ".getPath("dropbox-auth-start"));
        exit;
    }
    catch (dbx\WebAuthException_Csrf $ex) {
        respondWithError(403, "Unauthorized", "CSRF mismatch");
        // Write full details to server error log.
        // IMPORTANT: Never show the $ex->getMessage() string to the user -- it contains
        // sensitive information that could be used to bypass the CSRF check.
        error_log("/dropbox-auth-finish: CSRF mismatch: " . $ex->getMessage());
        exit;
    }
    catch (dbx\WebAuthException_NotApproved $ex) {
        echo renderHtmlPage("Not Authorized?", "Why not?");
        exit;
    }
    catch (dbx\WebAuthException_Provider $ex) {
        error_log("/dropbox-auth-finish: unknown error: " . $ex->getMessage());
        respondWithError(500, "Internal Server Error");
        exit;
    }
    catch (dbx\Exception $ex) {
        error_log("/dropbox-auth-finish: error communicating with Dropbox API: " . $ex->getMessage());
        respondWithError(500, "Internal Server Error");
        exit;
    }

    // NOTE: A real web app would store the access token in a database.
    $_SESSION['access-token'] = $accessToken;

    echo renderHtmlPage("Authorized!",
        "Auth complete, <a href='".htmlspecialchars(getPath(""))."'>click here</a> to browse.");
}

/*
 * Unlinks/logs out the Client from the App
 */
else if ($requestPath === "/dropbox-auth-unlink") {
    // "Forget" the access token.
    unset($_SESSION['access-token']);
    echo renderHtmlPage("Unlinked.",
        "Go back <a href='".htmlspecialchars(getPath(""))."'>home</a>.");
}

/*
 * For unknown request paths an error message is displayed
 */
else {
    echo renderHtmlPage("Bad URL", "No handler for $requestPath");
    exit;
}

/*
 * Renders the folder at the specified path, displaying the Dropbox contents and the files and folders specified as 
 * being in that directory by  the system file: '/.App_System_File'. Also displays a panel which allows the user to 
 * specify a file to upload and a checkbox for whether or not they want it to be uploaded "securely". A form
 * is also displayed for creating and removing folders.
 * 
 * @param string $path
 * 		representing the path to the folder being displayed in the browser
 * @param array $dbxContents
 * 		holding the contents of the folder in the Dropbox library (files & folders)
 */
function renderFolder($path, $dbxContents){
    // TODO: Add a token to counter CSRF attacks.
    $upload_path = htmlspecialchars(getPath('upload'));
    $path = htmlspecialchars($path);
    $form = <<<HTML
        <form action='$upload_path' method='post' enctype='multipart/form-data'>
        <label for='file'>Upload file:</label> <input name='file' type='file'/>
        Secure <input type="checkbox" name="secure"/>
        <input type='submit' value='Upload'/>
        <input name='folder' type='hidden' value='$path'/>
        </form>
HTML;
	$createFolderForm = <<<HTML
					<form>
			        <br><br>Folder:
			        <input type="text" name="folderName">
			        <input type='submit' name='submit' value='create'/>
			        <input type='submit' name='submit' value='delete' onclick="return confirm('Are you sure you want to delete this file?');"/>
			        <input name='path' type='hidden' value='$path'/>
			        </form>
HTML;

    //renders normal files in the folder. (System files (e.g. .file) are not displayed)
	$displayed = array();//stores folders displayed already, so they are displayed once
	$parentPath = dirname($path);
	$listing = "";
	
	//go to parent directory hyperlink
	if($path != "/"){
		if($parentPath === "/")
	 	   $listing = "<div><a style='text-decoration: none' href='".getPath("")."'>Parent Directory..</a></div>";
		else
			$listing = "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($parentPath))."'>Parent Directory..</a></div>";
	}
	
    if($dbxContents != null){
    	foreach ($dbxContents as $child) {
    		if($child['path']{1} != '.'){
        		$cp = $child['path'];
	        	$cn = basename($cp);
    	    	if ($child['is_dir']) $cn .= '/';

	        	$cp = htmlspecialchars($cp);
		        $link = getPath("?path=".htmlspecialchars($cp));
    		    $listing .= "<div><a style='text-decoration: none' href='$link'>$cn</a></div>";
    	    	if(!isFilePath($cn) && !in_array($cn, $displayed)){
    	    		array_push($displayed, $cn);
    	    	}
        	}
    	}
    }
    
    //renders "secure" files in the folder    
    $secure_listing = '<br>______________________________<br>________SECURE FILES:________<br>';
		
    $sysFileHandler = new App_System_File_Handler(getClient(), '/.App_System_File');
    
	if($sysFileHandler != null){
		$contents = $sysFileHandler->getContents();
	    foreach (array_keys($contents) as $file){
		    if(isIncluded($path, $file)){
	    		$tmp = getNextInPath($path, $file);
	    		if(!isFilePath($tmp))
	    			$tmp = $tmp."/";
	    		$link = getPath("?path=".htmlspecialchars($path.($path == "/"? "":"/").getNextInPath($path, $file)));
	    		if(!in_array($tmp, $displayed)){
	    			if(!isFilePath($tmp))
			    		array_push($displayed, $tmp);
			    	$secure_listing .= "<div><a style='text-decoration: none' href='$link'>$tmp</a></div>"; 
		    	}
    		}
    	}
    }

    return renderHtmlPage("Folder: $path", $form.$listing.$secure_listing.$createFolderForm);
}

/**
 * Echos a return hyperlink to the page which when clicked returns the user's browser to the $path's parent directory
 * 
 * @param string $path
 * 		the path which the user's browser should redirect to its parent directory when the hyperlink is clicked
 */
function echoReturnToParentLink($path){
	$parent = dirname($path);
	if($parent === "/")
		echo "<div><a style='text-decoration: none' href='".getPath("")."'>Return to Dropbox</a></div>";
	else
		echo "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($parent))."'>Return to Dropbox</a></div>";
}

/**
 * Echos a return hyperlink to the page which when clicked returns the user's browser to the $path
 * 
 * @param string $path
 * 		the path which the user's browser should redirect to when the hyperlink is clicked
 */
function echoReturnLink($path){
	if($path === "/")
		echo "<div><a style='text-decoration: none' href='".getPath("")."'>Return to Dropbox</a></div>";
	else
		echo "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($path))."'>Return to Dropbox</a></div>";
		
}

/**
 * Removes all of the "secure" files which are contained in the $folderPath. A boolean is
 * returned which is true if a file has been deleted and false if one has not.
 *
 * @param string $folderPath
 * 		path specifying the folder which all contained files are being removed from
 *
 * @return bool
 * 		true if a file has been deleted and false if one has not
 */
function removeSecureFilesInFolder($folderPath){
	$deleted = false;
	$hss = new Handle_Secure_Server();
	$sysFileHandler = new App_System_File_Handler(getClient(), '/.App_System_File');
    
	$contents = $sysFileHandler->getContents();
	foreach (array_keys($contents) as $file){
		 if(isIncluded($folderPath, $file)){
			$hss->delete($sysFileHandler->get($file));
			//$sysFileHandler = new App_System_File_Handler(getClient(), '/.App_System_File');
			$sysFileHandler->remove($file);
			$deleted = true;
    	}
    }
    return $deleted;
}

/**
 * Returns true if the $name is a valid folder name and false otherwise
 * 
 * @param string $name
 * 		the name of the folder in query
 * @param string $parent
 * 		the path which this folder is being created in
 *
 * @return bool
 * 		true if the $name is a valid folder name and false otherwise
 */
function isValidFolderName($name, $parent){
	$folderPath = ($parent === "/"? "":$parent)."/".$name;
	return !(strlen($folderPath) > 255 || strchr($name, '/') || strchr($name, '\\') || strchr($name, '<') || 
				strchr($name, '>') || strchr($name, ':') || strchr($name, '"') || 
				strchr($name, '|') || strchr($name, '?') || strchr($name, '*') || 
				strchr($name, '.'));
}

/**
 * Returns a string representing the folder or file in the current path, based on the whole path (i.e. to the actual file stored).
 * 
 * @param string $currentPath
 * 		representing the current path that the file or folder following is being returned from. This is the path to the directory the
 * 		browser should be displaying folders and files for
 * @param string $wholePath 
 * 		representing the whole path which is being examined for the file or folder following the currentPath. This is the path to the
 * 		actual file that is "securely" stored.
 *
 * @return string
 * 		representing the folder or file name in the current path, based on the whole path. This does not include
 * 		the '/' character at the end if it is a folder
 */
function getNextInPath($currentPath, $wholePath){
	$current = explode("/", $currentPath);
	$whole = explode("/", $wholePath);
	
	for($i = 0; $i < count($current); $i++){
		if($current[$i] == $whole[$i])
			unset($whole[$i]);
	}
	
	if($currentPath === "/"){
		return $whole[$i - 1];
	}else{
		return $whole[$i];
	}
}

/**
 * Returns a boolean which is true if the current path is fully in the whole path, false otherwise
 * 
 * @param string $currentPath
 * 		representing the current path which is being looked for completely in the $wholePath
 * @param string $wholePath
 * 		representing the whole path which the $currentPath is being looked for inside 
 *
 * @return bool
 * 		which is true if the current path is fully in the whole path, false otherwise
 */
 function isIncluded($currentPath, $wholePath){
    if($currentPath === "/") return true;
 	$current = explode("/", $currentPath);
	$whole = explode("/", $wholePath);
	
	for($i = 0; $i < count($current); $i++){
		if($current[$i] != $whole[$i])
			return false;
	}
	
	return true;
 }
 
 /**
  * Prints to the screen that the file name has been taken and has a link to return to the previous
  * directory in the file browser
  * 
  * @param string $path
  * 	path to the file that was being uploaded, which is used to return to the parent directory
  */
function fileNameTakenError($path){
	echo "Error: File name already exists.<br>";
	echoReturnToParentLink($path);
}

/**
 * Renders a normal Dropbox file, printing its Metadata to the screen and allowing the user to download it
 * 
 * @param Metadata $entry
 * 		Dropbox metadata for the file to render
 */
function renderNormalFile($entry)
{
    $metadataStr = htmlspecialchars(print_r($entry, true));
    $downloadPath = getPath("download?path=".htmlspecialchars($entry['path']));
    $deletePath = getPath("delete?path=".htmlspecialchars($entry['path']));
    $body = <<<HTML
        <pre>$metadataStr</pre>
        _____________________________________________________________________________________________<br>
        _____<a href="$downloadPath">Download this file</a>_______________________________________________________<a href="$deletePath" onclick="return confirm('Are you sure you want to delete this file?');">Delete this file</a>______
        
HTML;

    return renderHtmlPage("File: ".$entry['path'], $body);
}

/**
 * Renders a "secure" file, printing its Metadata to the screen and allowing the user to download it
 * 
 * @param string $path 
 * 		representing the path to the "secure" file in the Dropbox library
 */
function renderSecureFile($path)
{
	$sysFileHandler = new App_System_File_Handler(getClient(), '/.App_System_File');
	$hss = new Handle_Secure_Server();
	$encrypted_fileID = $sysFileHandler->get($path);
    $metadataStr = $hss->getMetadata($encrypted_fileID);
    $downloadPath = $hss->getFileDownloadLink($encrypted_fileID);
    $deletePath = getPath("delete?path=".htmlspecialchars($path));
    $body = <<<HTML
        <pre>$metadataStr</pre>
        _____________________________________________________________________________________________<br>
        _____<a href="$downloadPath">Download this file</a>_______________________________________________________<a href="$deletePath" onclick="return confirm('Are you sure you want to delete this file?');">Delete this file</a>______
        
HTML;

    return renderHtmlPage("File: ".$path, $body);
}

/**
 * Returns true if the $path is to a file (i.e. there is a '.' character before the first '/', from right
 * to left), false otherwise.
 * 
 * @param string $path
 * 		path to the file
 * 
 * @return bool 
 * 		true if the $path is to a file (i.e. there is a '.' character before the first '/', from right
 * 		to left), false otherwise.
 */
 function isFilePath($path){
 	for($i = (strlen($path) - 1); $i >= 0; $i--){
 		if($path{$i} == '.') return true;
 		if($path{$i} == '/') return false;
 	}
 	return false;
 }
 
/**
 * Returns an array containing the AppInfo, client identifier and user locale
 * 
 * @return array 
 * 		containing the AppInfo, client identifier and user locale
 */
 function getAppConfig()
{
    global $appInfoFile;

    try {
        $appInfo = dbx\AppInfo::loadFromJsonFile($appInfoFile);
    }
    catch (dbx\AppInfoLoadException $ex) {
        throw new Exception("Unable to load \"$appInfoFile\": " . $ex->getMessage());
    }

    $clientIdentifier = "examples-web-file-browser";
    $userLocale = null;

    return array($appInfo, $clientIdentifier, $userLocale);
}

/**
 * Returns the Dropbox Client which is logged in
 * 
 * @param Client
 * 		the Dropbox Client which is logged in
 */
function getClient()
{
    if (!isset($_SESSION['access-token'])) {
        return false;
    }

    list($appInfo, $clientIdentifier, $userLocale) = getAppConfig();
    $accessToken = $_SESSION['access-token'];
    return new dbx\Client($accessToken, $clientIdentifier, $userLocale, $appInfo->getHost());
}

/**
 * Returns the WebAuth for this App
 * 
 * @return WebAuth 
 * 		for this App
 */
function getWebAuth()
{
    list($appInfo, $clientIdentifier, $userLocale) = getAppConfig();
    $redirectUri = getUrl("dropbox-auth-finish");
    $csrfTokenStore = new dbx\ArrayEntryStore($_SESSION, 'dropbox-auth-csrf-token');
    return new dbx\WebAuth($appInfo, $clientIdentifier, $redirectUri, $csrfTokenStore, $userLocale);
}

/**
 * Renders the HTML page with the given $title and $body
 * 
 * @param string $title
 * 		the title of the HTML page
 * @param string $body
 * 		the body of text for the HTML page
 */
function renderHtmlPage($title, $body)
{
    return <<<HTML
    <html>
        <head>
            <title>$title</title>
        </head>
        <body>
            <h1>$title</h1>
            $body
        </body>
    </html>
HTML;
}

/**
 * Responds to an error by redirecting the browser and rendering an error message
 * 
 * @param int $code
 * 		code given as an ID for the error message
 * @param string $title
 * 		title to be displayed on the HTML error message
 * @param string $body
 * 		body of text to be displayed with the HTML error message. Default is blank.
 */
function respondWithError($code, $title, $body = "")
{
    $proto = $_SERVER['SERVER_PROTOCOL'];
    header("$proto $code $title", true, $code);
    echo renderHtmlPage($title, $body);
}

/**
 * Returns the URL based on the $relative_path
 * 
 * @param string $relative_path
 * 		relative path to base the URL on
 * 
 * @return string
 * 		URL based on the relative path
 */
function getUrl($relative_path)
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
        $scheme = "https";
    } else {
        $scheme = "http";
    }
    $host = $_SERVER['HTTP_HOST'];
    $path = getPath($relative_path);
    return $scheme."://".$host.$path;
}

/**
 * Returns the path based on the $relative_path. This is used to redirect pages.
 *
 * @param string $relative_path
 * 		the relative path to base the path returned on
 *
 * @return string
 * 		the path based on the $relative_path
 */
function getPath($relative_path)
{
    if (PHP_SAPI === 'cli-server') {
        return "/".$relative_path;
    } else {
        return $_SERVER["SCRIPT_NAME"]."/".$relative_path;
    }
}

/**
 * Initialisation method
 * 
 * @return string
 * 		path/script name based on the server this file is being launched in
 */
function init()
{
    global $argv;

    // If we were run as a command-line script, launch the PHP built-in web server.
    if (PHP_SAPI === 'cli') {
        launchBuiltInWebServer($argv);
        assert(false);
    }

    if (PHP_SAPI === 'cli-server') {
        // For when we're running under PHP's built-in web server, do the routing here.
        return $_SERVER['SCRIPT_NAME'];
    }
    else {
        // For when we're running under CGI or mod_php.
        if (isset($_SERVER['PATH_INFO'])) {
            return $_SERVER['PATH_INFO'];
        } else {
            return "/";
        }
    }
}

/**
 * Lanches a built in web server used for running the web-file-browser as a script. NOTE: This has never been tested for
 * my App.
 * 
 * @param array $argv
 * 		arguements for the built in web server
 */
function launchBuiltInWebServer($argv)
{
    // The built-in web server is only available in PHP 5.4+.
    if (version_compare(PHP_VERSION, '5.4.0', '<')) {
        fprintf(STDERR,
            "Unable to run example.  The version of PHP you used to run this script (".PHP_VERSION.")\n".
            "doesn't have a built-in web server.  You need PHP 5.4 or newer.\n".
            "\n".
            "You can still run this example if you have a web server that supports PHP 5.3.\n".
            "Copy the Dropbox PHP SDK into your web server's document path and access it there.\n");
        exit(2);
    }

    $php_file = $argv[0];
    if (count($argv) === 1) {
        $port = 5000;
    } else if (count($argv) === 2) {
        $port = intval($argv[1]);
    } else {
        fprintf(STDERR,
            "Too many arguments.\n".
            "Usage: php $argv[0] [server-port]\n");
        exit(1);
    }

    $host = "localhost:$port";
    $cmd = escapeshellarg(PHP_BINARY)." -S ".$host." ".escapeshellarg($php_file);
    $descriptors = array(
        0 => array("pipe", "r"),  // Process' stdin.  We'll just close this right away.
        1 => STDOUT,              // Relay process' stdout to ours.
        2 => STDERR,              // Relay process' stderr to ours.
    );
    $proc = proc_open($cmd, $descriptors, $pipes);
    if ($proc === false) {
        fprintf(STDERR,
            "Unable to launch PHP's built-in web server.  Used command:\n".
            "   $cmd\n");
        exit(2);
    }
    fclose($pipes[0]);  // Close the process' stdin.
    $exitCode = proc_close($proc);  // Wait for process to exit.
    exit($exitCode);
}

?>