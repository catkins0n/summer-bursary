<?php

require_once __DIR__.'/../../lib/Google/autoload.php';

/**
 * Manipulates a Google Drive library ("secure" library). If the client is not logged into Google
 * or the client's access token has expired (after ~60 minutes) the browser is redirected to '/Authorize_Google.php' 
 * If redirected, after the access token is set, the browser is redirected to '/web-file-browser.php' and the request
 * needs to be recarried out by the user.
 * 
 * IMPORTANT: during set up, read the 'IMPORTANT' field in the '/Authorize_Google.php' file found in the README.md file.
 * 
 * @author Craig Atkinson
 * @date 05/12/2014
 */
class Handle_Secure_Server{

	/**
	 * Uploads the file at $_FILES['file']['name'] to the Secure Google Drive, returning the file's ID given by Google Drive
	 * to specify the file. 
	 * 
	 * @return string
	 * 		representing the ID of the file which has been uploaded to Google Drive
	 */
	public function uploadFile(){
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		
	 	$file = new Google_Service_Drive_DriveFile();
		$file->title = $_FILES['file']['name'];
		$file->properties = array($this->makeProperty("Outsourced_to_Dropbox", "true", "PRIVATE"));
		$chunkSizeBytes = 1 * 1024 * 1024;

		// Call the API with the media upload, defer so it doesn't immediately return.
		$client->setDefer(true);
		
		try{
			$request = $service->files->insert($file);
		}catch(Google_Auth_Exception $e){
			unset($_SESSION['access_token']);
			$this->uploadFile();
			return null;
		}
		// Create a media file upload to represent our upload process.
		$media = new Google_Http_MediaFileUpload(
  			$client,
	  		$request,
  			'text/plain',
  			null,
	  		true,
  			$chunkSizeBytes
		);
		$media->setFileSize(filesize($_FILES['file']['tmp_name']));

		// Upload the various chunks. $status will be false until the process is
		// complete.
		$status = false;
		$handle = fopen($_FILES['file']['tmp_name'], "rb");
		while (!$status && !feof($handle)) {
		  	$chunk = fread($handle, $chunkSizeBytes);
  			$status = $media->nextChunk($chunk);
		}

		// The final value of $status will be the data from the API for the object
		// that has been uploaded.
		$result = false;
		if($status != false) {
  			$result = $status;
		}

		fclose($handle);
		// Reset to the client to execute requests immediately in the future.
		$client->setDefer(false);
		
		return $this->encrypt($result->id, $this->getKey(".App_System_File", $service));
	}
	
	/**
	 * Permanently deletes the file from the Secure Server
	 *
	 * @param string $encrypted_fileID
	 * 		encrypted ID specifying the file which is being removed from the Secure Server
	 */
	public function delete($encrypted_fileID){
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		$fileID = $this->decrypt($encrypted_fileID, $this->getKey(".App_System_File", $service));
		$service->files->delete($fileID);		
	}
	
	/**
	 * Encrypts the the $plaintext using mcrypt, returning the cipher text representing it
	 * WARNING === Resulting cipher text has no integrity or authenticity added
     * and is not protected against padding oracle attacks.
	 * 
	 * @param string $plaintext
	 * 		representing the text to be encrypted
	 * 
	 * @param hexadecimal number $key
	 * 		32 byte cipher key being used to encrypt the $plaintext
	 * 
	 * @return string (base64 encoded)
	 * 		the cipher text which is an ecrypted representation of the $plaintext
	 */
	private function encrypt($plaintext, $key){
	
		# create a random IV to use with CBC encoding
   		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    
    	# creates a cipher text compatible with AES (Rijndael block size = 128)
    	# to keep the text confidential 
    	# only suitable for encoded input that never ends with value 00h
    	# (because of default zero padding)
    	$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
                                 $plaintext, MCRYPT_MODE_CBC, $iv);

    	# prepend the IV for it to be available for decryption
    	$ciphertext = $iv . $ciphertext;
    
    	# encode the resulting cipher text so it can be represented by a string
    	$ciphertext_base64 = base64_encode($ciphertext);

    	return  $ciphertext_base64;
	}
	
	/**
	 * Decrypts the $ciphertext_base64 into the plain text represention
	 * 
	 * @param string $ciphertext_base64
	 * 		representing the text to be decrypted (base64 encoded)
	 * 
	 * @param hexadecimal number $key
	 * 		32 byte cipher key being used to decrypt the $ciphertext_base64
	 * 
	 * @return string
	 * 		the plain text which is a decrypted representation of the $ciphertext_base64
	 */
	private function decrypt($ciphertext_base64, $key){
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	
		$ciphertext_dec = base64_decode($ciphertext_base64);
    
    	# retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
    	$iv_dec = substr($ciphertext_dec, 0, $iv_size);
    
    	# retrieves the cipher text (everything except the $iv_size in the front)
    	$ciphertext_dec = substr($ciphertext_dec, $iv_size);

    	# may remove 00h valued characters from end of plain text
    	$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
                                    $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
                                    
    	$plaintext_dec = str_replace("\0", "", $plaintext_dec);
    	return  $plaintext_dec;
	 }
	
	/**
 	 * Returns a Google_Service_Drive_Property which resembles the input parameters
 	 *
	 * @param string $key
	 * 		ID of the property.
	 * @param string $value 
	 * 		property value.
	 * @param string $visibility
	 * 		'PUBLIC' to make the property visible by all apps, or 'PRIVATE' to make it only
	 * 		available to the app that created it.
	 * 
	 * @return Google_Service_Drive_Property
	 * 		which resembles the input parameters
	 */
	private function makeProperty($key, $value, $visibility){
		$newProperty = new Google_Service_Drive_Property();
	  	$newProperty->setKey($key);
  		$newProperty->setValue($value);
	  	$newProperty->setVisibility($visibility);
  		return $newProperty;
	}
	
	/**
	 * Returns the $file's property/value with the specified $key, 
	 * null is returned if the $key is not found
	 * 
	 * @param Google_Service_Drive_DriveFile $file
	 * 		file which the property is being extracted from
	 * @param string $key
	 * 		key which specifies the property being returned
	 *
	 * @return string
	 * 		Contents of the $file's property with the specified $key, 
	 * 		null is returned if the $key is not found
	 */
	private function getProperty($file, $key){
	foreach ($file->properties as $tmp){
		if($tmp['key'] === $key)
			return $tmp['value'];
	}
		return null;
	}
	
	/**
	 * Returns the Google_Client authenticated with $_SESSION['access_token']. If its not set, 
	 * The browser is redirected to '/Authorize_Google.php' and then redirected to the
	 * '/web-file-browser.php', to manually redo the request.
	 * 
	 * @return Google_Client
	 * 		which is authenticated (by Google) or null if none is set
	 */
	 private function getClient(){
	 	if (!isset($_SESSION['access_token'])) {
			header("Location: " . $this->getAuthURL());
		}
		$client = new Google_Client();
		$client->setAccessToken($_SESSION['access_token']);
	 	return $client;
	 }
	
	/**
	 * Returns the Metadata for the file specified by the $encrypted_fileID
	 * 
	 * @param string $encrypted_fileID
	 * 		encrypted ID for the file in Google Drive
	 * 
	 * @return string 
	 * 		representing the specified file's Metadata
	 */
	 public function getMetadata($encrypted_fileID){
	 	$file = $this->getFile($encrypted_fileID);
	 	if($file == null) return null;
    	return '<pre>'. print_r($file, true). '</pre>';
	 }
	 
	 /**
	  * Returns the download link to the file specified by the $encrypted_fileID
	  * 
	  * @param string $encrypted_fileID
	  * 	encrypted ID for the file in Google Drive
	  * 
	  * @return string
	  * 	download link to the file specified by the $encrypted_fileID
	  */
	 public function getFileDownloadLink($encrypted_fileID){
		$file = $this->getFile($encrypted_fileID);
	 	return $file->getWebContentLink();
	 }
	
	/**
	 * Returns the Google Drive File specified by the $encrypted_fileID, null is returned if an error occurs
	 * 
	 * @param string $encrypted_fileID
	 * 		encrypted ID specifying the file in the Google Drive library
	 * 
	 * @return Google_Service_Drive_DriveFile
	 * 		specified by the $encrypted_fileID, null is returned if an error occurs
	 */
	 private function getFile($encrypted_fileID){
	 	$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		$fileID = $this->decrypt($encrypted_fileID, $this->getKey(".App_System_File", $service));
		try{
			return $service->files->get($fileID);
		}catch(Google_Auth_Exception $e){
			unset($_SESSION['access_token']);
			header("Location: " . $this->getAuthURL());
		}
	 }
	 
	 /**
	  * Returns the cipher key from the App's system file properties (on Google Drive). For efficiency it attempts to access it with the 
	  * sessions 'sysFileID'. But if this is not set or successful all the user's Google Drive files are searched for the file and if this
	  * is not successful: a new system file is created. 
	  * 
	  * @param string $appFileName
	  * 	name of the system file storing the cipher key on the Google Drive server
 	  * @param Google_Service_Drive $service
	  * 	representing the client's Google_Service_Drive
	  * 
	  * @return hexadecimal number
	  * 	32 byte cipher key from the App's system file (on Google Drive)
	  */
	 private function getKey($appFileName, $service){
	 	if(isset($_SESSION['sysFileID'])){
	 		$sysFile = $this->getFile($_SESSION['sysFileID']);
	 		if($sysFile != null) return $this->getProperty($sysFile, 'Encryption_key');
	 	}
		$sysFile = $this->getSystemFile($appFileName, $service);
		return $this->getProperty($sysFile, 'Encryption_key');
	 }
	 
	 /**
	  * Returns the Secure Server's system file in the root directory specified (containing the key), if none
	  * exists one is created
	  * 
	  * @param string $appFileName
	  * 	name of the system file storing the cipher key in the client's Google Drive library
	  * @param Google_Service_Drive $service
	  * 	representing the client's Google_Service_Drive
	  * 
	  * @return Google_Service_Drive_DriveFile
	  * 	system file in the root directory specified (containing the 32 byte cipher key in its properties), 
	  * 	if none exists one is created
	  */
	private function getSystemFile($appFileName, $service){
		$allFiles = $this->retrieveAllFiles($service);
		foreach($allFiles as $file ){
			$labels = $file->getLabels();
			if(!$labels['trashed'] && $file['title'] == $appFileName){
				$_SESSION['sysFileID'] = $file['fileId'];
				return $file;
			}
		}
		return $this->createSystemFile($appFileName, $service);
	}
	
	/**
	 * Creates a system file in the root directory of the Secure Server, with the name specified. The system file
	 * contains a randomly generated 32 byte cipher key in its properties. The file created is then returned.
	 * 
	 * @param string $appFileName
	 * 		name of the system file to create and store the cipher key in
	 * @param Google_Service_Drive $service
	 * 		representing the client's Google_Service_Drive
	 * 
	 * @return Google_Service_Drive_DriveFile
	 *		the system file created in the root directory. The system file has the name/title 
	 * 		specified and contains the 32 byte cipher key in its properties
	 */
	private function createSystemFile($appFileName, $service){
		$file = new Google_Service_Drive_DriveFile();
		$file->setTitle($appFileName);
		$file->setDescription('DO NOT DELETE: this file is being used by a Dropbox App to secure files there by storing them in the Google Drive library. This file is essential to you being able to access them via the Dropbox App');
		$file->properties = array($this->makeProperty("Outsourced_to_Dropbox", "true", "PRIVATE"), $this->makeProperty("Encryption_key", $this->getRandomCipherKey(), "PRIVATE"));
		$file->setMimeType('text/plain');

		$data = 'DO NOT DELETE: this file is being used by a Dropbox App to secure files there by storing them in the Google Drive library. This file is essential to you being able to access them via the Dropbox App';

		$createdFile = $service->files->insert($file, array(
      		'data' => $data,
      		'mimeType' => 'text/plain',
      		'uploadType' => 'media'
		));
		$_SESSION['sysFileID'] = $createdFile['fileId'];
		return $createdFile;
	}
	
	/**
	 * Returns a randomly generated 32 byte cipher key
	 * 
	 * @return hexadecimal number
	 * 		randomly generated 32 byte cipher key
	 */
	 private function getRandomCipherKey(){
	 	return md5(uniqid(mt_rand(), false));
	 }
	 
	/**
 	 * Retrieve a list of File resources owned by the client
 	 *
 	 * @param Google_DriveService $service
 	 * 		Drive API service instance
 	 * 
 	 * @return array
 	 * 		representing Google_DriveFile resources
 	 */
	private function retrieveAllFiles($service) {
		$result = array();
		$pageToken = NULL;
		
		do {
    		try {
      			$parameters = array();
      			if ($pageToken) {
        			$parameters['pageToken'] = $pageToken;
     			}
      			$files = $service->files->listFiles($parameters);

      			$result = array_merge($result, $files->getItems());
			    $pageToken = $files->getNextPageToken();
			} catch (Google_Auth_Exception $e) {
				unset($_SESSION['access_token']);
				header("Location: " . $this->getAuthURL());
			}
		} while ($pageToken);
		return $result;
	}
	
	/**
	 * Returns true if the access token has expired and false if it is still valid.
	 * 
	 * @return bool
	 * 		true if the access token has expired and false if it is still valid
	 */
	public function isAccessTokenExpired(){
	 	return $this->getClient()->isAccessTokenExpired();
	}
	
	/**
	 * Returns a string specifying the URL path on the server to the file which authorizes 
	 * the app's access to the secure server
	 *
	 * @return string 
	 * 		specifying the URL path on the server to the file which authorizes 
	 * 		the app's access to the secure server
	 */
	 public function getAuthURL(){
	 	return "http://127.0.0.1:4567/src/Secure_Dropbox/Authorize_Google.php";
	 }
}	

?>