<?php

	require_once __DIR__."/../../../lib/Dropbox/strict.php";
	require_once __DIR__.'/../../../lib/Dropbox/autoload.php';
	require_once __DIR__.'/../../../lib/mylib.php';
	require_once __DIR__.'/../App_System_File_Handler.php';

	use \Dropbox as dbx;
	
	/*Read App information path*/
	$appInfoPath = "AppInfo.json";
	
	/*Read Client information path*/
	$clientInfoPath = "356509711.json";
	
	/*File path to be added or removed*/
	$loadFilePath = __DIR__."/.my_dropbox_app";
	$storeFilePath = "/.my_dropbox_app";
	
	$accessToken = getJSONField('access_token', $clientInfoPath);
	$appName = getJSONField('app_name', $appInfoPath);

	/*Make Dropbox Client*/
  	$dbxClient = new dbx\Client($accessToken, $appName);
  	
  	
  	//echo $dbxClient . "      " . 'sys.App_System_File';
  	$sfh = new App_System_File_Handler($dbxClient, '/.App_System_File');
  	$sfh->add("name", "Craig");
  	$sfh->print_contents();
  	
  	/*Upload file*/
	/*$f = fopen($loadFilePath, "rb");
	$result = $dbxClient->uploadFile($storeFilePath, dbx\WriteMode::add(), $f);
	fclose($f);
	echo "Added: $storeFilePath";
	echo '<br>';
	echo '<br>';
	print_r($result);
	echo '<br>';
	*/
	
	/*$fd = fopen(__DIR__."/hi.txt", "wb");//"chmod 777 /vagrant/src/test" command to work (IN THE VAGRANT DIRECTORY) not sure which flags i need wba?
	$meta = $dbxClient->getFile($storeFilePath, $fd, null);
	fclose($fd);*/
	//$f1 = getJSONField('google', $loadFilePath);
	//echo "$f1";
	
	/*echo '<br>';
	$result = $dbxClient->delete($storeFilePath);
	echo "Deleted: $storeFilePath";
	echo '<br>';*/
	
	
	/*$file = array(
			"/hi.txt" => "www.facebook.com",
			"/folder/bye.txt" => "www.google.com"
		);
		
	$file2 = array(
			"/hi.txt" => "www.facebook.com",
			"/folder/bye.txt" => "www.google.com"
		);
	
	array_push($file, $file2);
	
	$json_options = 0;
	if (defined('JSON_PRETTY_PRINT')) {
   			$json_options |= JSON_PRETTY_PRINT;  // Supported in PHP 5.4+
	}
	$json = json_encode($file, $json_options);
	*/

	//$dbxClient->uploadFileFromString($storeFilePath, dbx\WriteMode::add(), $json);
		
	
	
	////////////////////////////////////////////////////////////////upload file from string will be helpful
	//	to allow moving of files can use searchFileNames and then compare revisions maybe?
?>