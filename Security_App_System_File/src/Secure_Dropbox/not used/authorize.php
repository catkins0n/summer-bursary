<?php

	require_once __DIR__."/../../../lib/Dropbox/strict.php";
	require_once __DIR__.'/../../../lib/Dropbox/autoload.php';
	require_once __DIR__.'/../../../lib/mylib.php';

	use \Dropbox as dbx;

	/*Read App information path*/
	$appInfoPath = "AppInfo.json";

	$appName = getJSONField('app_name', $appInfoPath);

	/*Get App information*/
	$appInfo = dbx\AppInfo::loadFromJsonFile($appInfoPath);
	$webAuth = new dbx\WebAuthNoRedirect($appInfo, $appName);

	usage($webAuth->start());
	
	/*if authentication code submitted the access token is saved*/
	if(isset($_POST['code'])){
		$authCode = $_POST['code'];
		list($accessToken, $dropboxUserId) = $webAuth->finish($authCode);
		$success = writeAccessToken($accessToken, $dropboxUserId);
		if($success){
			/*Redirect to /////////////////////////////////////////////////////////////////*/
			header("Location: ../../index.php");	
		}
	}
?>

<form action = "authorize.php" method = "POST">
<input type = "password" name = "code" />
<input type = "submit" value = "submit" />
</form>

<?php

	/**
	 * Echos message prompting the user to authenicate App
	 * 
	 * @param string $authorizedUrl
	 *  	URL which the authorization code is at
	 */
	function usage($authorizeUrl){
		echo "<strong>Before using this Dropbox App you must authenticate it with your account</strong>";
		echo '<br>';
		echo '<br>';
		echo "1. Go to: <strong>" . $authorizeUrl . "</strong>";
		echo '<br>';
		echo "2. Click \"Allow\" (you might have to log in first).";
		echo '<br>';
		echo "3. Copy the authorization code.";
		echo '<br>';
		echo '<br>';
		echo "<strong>Enter the authorization code here:</strong>";
		echo '<br>';
	}
	
	/**
	 * Writes the access token to a file named with the users ID.
	 * True is returned if the write is successful and false otherwise
	 * 
	 * @param string $accessToken
	 *  	User's access token for the App
	 * @param string $userID
	 * 		User's ID which will be used as a file name to save the
	 * 		.json file at
	 * 
	 * @return boolean
     *    true if write successful, false otherwise
	 */
	 function writeAccessToken($accessToken, $userID){
	 	$file = array(
			"access_token" => $accessToken
		);
	
		$json_options = 0;
		if (defined('JSON_PRETTY_PRINT')) {
   	 		$json_options |= JSON_PRETTY_PRINT;  // Supported in PHP 5.4+
		}
		$json = json_encode($file, $json_options);

		if (file_put_contents($userID . ".json", $json) !== false) {
		   	return true;
		}
		
		echo '<br>';echo '<br>';echo '<br>';
		echo "<strong>Error saving Access Token!</strong>";
		echo '<br>';
		echo "Please try again.";
		echo '<br>';echo '<br>';echo '<br>';echo '<br>';
		return false;
	 }
?>