<?php

require_once __DIR__."/../../lib/Dropbox/strict.php";
require_once __DIR__.'/../../lib/Dropbox/autoload.php';

use \Dropbox as dbx;


/**
 * Creates and manipulates a system file (e.g. '/.App_System_File'). This file holds a json encoded represention of
 * an associative array, storing the path a "secured" Dropbox file should be stored at and the fileID of where it is
 * actually stored at, on the secure server.
 * 
 * @author Craig Atkinson
 * @date 05/12/2014 
 */
class App_System_File_Handler{

	/**
	 * Dropbox Client
	 */
	private $client;

	/**
	 * System file path
	 */
	private $filePath;

	/**
	 * Revision of the file
	 */
	 private $rev;

	/**
	 * Contents of the system file
	 */
	private $contents = array();

	/**
	 * Creates an instance App_System_File_Handler by loading the contents of its specified system file or creating
	 * a new one if none is present
	 *
	 * @param Client $client 
	 * 		Dropbox client which the system file is being read from
	 * @param string $filePath 
	 * 		representing the path of the system file being loaded and handled by this instance
	 */
	public function __construct($client, $filePath){
		if($client != null){
			$this->client = $client;
			$this->filePath = $filePath;
			$this->load();
		}
	}
	
	/**
	 * Creates a system file in the user's Dropbox at $this->filePath with the specified contents ($this->contents)
	 * 
	 * @return array 
	 * 		representing the metadata of the file uploaded
	 */
	 private function createFile(){
		$json = json_encode($this->contents);
		return $this->client->uploadFileFromString($this->filePath, dbx\WriteMode::add(), $json);
	 }
	 
	/**
	 * Saves the system file to the user's Dropbox (via updating it) at $this->filePath with the specified contents ($this->contents)
	 */
	 private function save(){
		$json = json_encode($this->contents);
		$this->client->uploadFileFromString($this->filePath, dbx\WriteMode::update($this->rev), $json);
		$this->load();
	 }
	 
	/**
	 * Loads the system file from the user's Dropbox saving its information to $this->contents. If the file doesn't exist one is created
	 */
	private function load(){
		$fd = tmpfile();
	    $metadata = $this->client->getFile($this->filePath, $fd);
	    if($metadata == null){
	    	$metadata = $this->createFile();
	    }else{
	    	fseek($fd, 0);
    		$this->contents = json_decode(fread($fd, $metadata['bytes']), true);
	    }
	    $this->rev = $metadata['rev'];
	    fclose($fd);
	}
	
	/**
	 * Adds the key or changes the value for the existing key
	 * 
	 * @param string $key 
	 * 		which is being added with a value or having its current value manipulated
	 * @param string $value 
	 * 		value being assigned to the key 
	 */
	public function add($key, $value){
		$this->contents[$key] = $value;
		$this->save();
	}
	
	/**
	 * Removes the key and its value from the system file
	 * 
	 * @param string $key 
	 * 		which is being removed from the system file
	 */
	public function remove($key){
		unset($this->contents[$key]);
		$this->save();
	}
	
	/**
	 * Returns true if the system file contains the key and false otherwise
	 * 
	 * @param string $key 
	 * 		which is being searched for in the system file
	 * 
	 * @return bool
	 * 		which is true if the system file contains the key and false otherwise
	 */
	public function containsKey($key){
		return array_key_exists($key, $this->contents);
	}
	
	/**
	 * Returns true if the system file contains the value and false otherwise
	 * 
	 * @param string $value 
	 * 		which is being searched for in the system file
	 * 
	 * @return bool
	 * 		which is true if the system file contains the value and false otherwise
	 */
	public function containsValue($value){
		return in_array($value, $this->contents);
	}
	
	/**
	 * Returns the value associated with the key in the system file
	 * 
	 * @param string $key 
	 * 		which is being searched for in the system file
	 * 
	 * @return string
	 * 		representing the value associated with the key in the system file
	 */
	public function get($key){
		return $this->contents[$key];
	}
	
	/**
	 * Returns the contents of the system file as an associative array
	 * 
	 * @return array
	 * 		contents of the system file as an associative array
	 */
	 public function getContents(){
	 	return $this->contents;
	 }

	/**
	 * Prints the contents of the system file
	 */
	public function print_contents(){
		echo '<pre>', print_r($this->contents, true), '</pre>';
	}

}

?>