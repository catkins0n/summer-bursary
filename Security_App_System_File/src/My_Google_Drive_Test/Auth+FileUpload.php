<?php
require_once __DIR__.'/../../lib/Google/autoload.php';
include_once "templates/base.php";
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 session_start();

/************************************************
  ATTENTION: Fill in these values! Make sure
  the redirect URI is to this page, e.g:
  http://localhost:8080/user-example.php
 ************************************************/
$client_id = '<App_key>';
$client_secret = '<App_secret>';
$redirect_uri = 'http://127.0.0.1:4567/src/My_Google_Drive_Test/Auth+FileUpload.php';

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setScopes(array(
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'));

/************************************************
  If we're logging out we just need to clear our
  local access token in this case
 ************************************************/
 /////////////////////////////////////////////////////////////////////////////////
 ///////////uncomment the following line if your accesss token has expired////////
 /////////////////////then run, then comment out and run again////////////////////
 /////////////////////////////////////////////////////////////////////////////////
 //unset($_SESSION['access_token']);
if (isset($_REQUEST['logout'])) {
  	unset($_SESSION['access_token']);
}

/************************************************
  If we have a code back from the OAuth 2.0 flow,
  we need to exchange that with the authenticate()
  function. We store the resultant access token
  bundle in the session, and redirect to ourself.
 ************************************************/
if (isset($_GET['code'])) {
  	$client->authenticate($_GET['code']);
 	 $_SESSION['access_token'] = $client->getAccessToken();
  	$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  	header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

/************************************************
  If we have an access token, we can make
  requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessToken($_SESSION['access_token']);
  	$service = new Google_Service_Drive($client);
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////vvvv
  //between these two markers is code not needed for authentication but tests uploading a file and changing its properties
 	$file = new Google_Service_Drive_DriveFile();
	$file->title = "Big File.txt";
	/*$file->labels = array(
		"starred" => true,
		"hidden" => false,
		"trashed" => false,
		"restricted" => true,
		"viewed" => false
	) ;*/
	$file->properties = array(getProperty("MYPROP", "IMSET", "PRIVATE"));
	$chunkSizeBytes = 1 * 1024 * 1024;

	// Call the API with the media upload, defer so it doesn't immediately return.
	$client->setDefer(true);
	$request = $service->files->insert($file);

	// Create a media file upload to represent our upload process.
	$media = new Google_Http_MediaFileUpload(
  		$client,
  		$request,
  		'text/plain',
  		null,
  		true,
  		$chunkSizeBytes
	);
	$media->setFileSize(filesize(__DIR__."/ExampleFile.txt"));

	// Upload the various chunks. $status will be false until the process is
	// complete.
	$status = false;
	$handle = fopen(__DIR__."/ExampleFile.txt", "rb");
	while (!$status && !feof($handle)) {
  	$chunk = fread($handle, $chunkSizeBytes);
  	$status = $media->nextChunk($chunk);
 }

// The final value of $status will be the data from the API for the object
// that has been uploaded.
$result = false;
if($status != false) {
  	$result = $status;
}

fclose($handle);
// Reset to the client to execute requests immediately in the future.
$client->setDefer(false);

//print_r($result);
renameFile($service, $result->id, "HIIIIII.txt");
		echo '<br>';
		echo '<br>';
printFile($service, $result->id);
///////////////////////////////////////////////////////////////////////////////////////////////////////////^^^^
} else {
  	$authUrl = $client->createAuthUrl();
}

/************************************************
  If we're signed in we can go ahead and retrieve
  the ID token, which is part of the bundle of
  data that is exchange in the authenticate step
  - we only need to do a network call if we have
  to retrieve the Google certificate to verify it,
  and that can be cached.
 ************************************************/
if ($client->getAccessToken()) {
	$_SESSION['access_token'] = $client->getAccessToken();
  	$token_data = $client->verifyIdToken()->getAttributes();
}

echo pageHeader("User Query - Retrieving An Id Token");
if (
		$client_id == '<YOUR_CLIENT_ID>'
    	|| $client_secret == '<YOUR_CLIENT_SECRET>'
    	|| $redirect_uri == '<YOUR_REDIRECT_URI>'
    ) {
  	echo missingClientSecretsWarning();
}
?>
<div class="box">
  	<div class="request">
<?php
if (isset($authUrl)) {
  	echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
} else {
  	echo "<a class='logout' href='?logout'>Logout</a>";
}
?>
  	</div>

  	<div class="data">
<?php 
if (isset($token_data)) {
  //////////////////////////////////////////////////////////////////////////////var_dump($token_data);
}
?>
  	</div>
</div>


<?php

/**
 * Insert a new custom file property.
 *
 * @param Google_DriveService $service Drive API service instance.
 * @param String $fileId ID of the file to insert property for.
 * @param String $key ID of the property.
 * @param String $value Property value.
 * @param String $visibility 'PUBLIC' to make the property visible by all apps,
 *                           or 'PRIVATE' to make it only available to the app that created it.
 * @return Google_Property The inserted property. NULL is returned if an API
                           error occurred.
 */
function insertProperty($service, $fileId, $key, $value, $visibility) {
 	$newProperty = new Google_Service_Drive_Property();
  	$newProperty->setKey($key);
  	$newProperty->setValue($value);
  	$newProperty->setVisibility($visibility);
  	try {
    	return $service->properties->insert($fileId, $newProperty);
  	} catch (Exception $e) {
    	print "An error occurred: " . $e->getMessage();
  	}
  	return NULL;
}

/**
 * Returns a Google_Service_Drive_Property which resembles the input parameters
 *
 * @param String $key ID of the property.
 * @param String $value Property value.
 * @param String $visibility 'PUBLIC' to make the property visible by all apps,
 *                           or 'PRIVATE' to make it only available to the app that created it.
 * @return Google_Service_Drive_Property which resembles the input parameters
 */
function getProperty($key, $value, $visibility){
	$newProperty = new Google_Service_Drive_Property();
  	$newProperty->setKey($key);
  	$newProperty->setValue($value);
  	$newProperty->setVisibility($visibility);
  	return $newProperty;
}

/**
 * Print a file's metadata.
 *
 * @param apiDriveService $service Drive API service instance.
 * @param string $fileId ID of the file to print metadata for.
 */
function printFile($service, $fileId) {
  try {
    $file = $service->files->get($fileId);

    print "Title: " . $file->getTitle();
    print "Description: " . $file->getDescription();
    print "MIME type: " . $file->getMimeType();
    print_r($file->getProperties());
    print("Labels: ");
    print_r($file->labels);
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
}

/**
 * Download a file's content.
 *
 * @param apiDriveService $service Drive API service instance.
 * @param File $file Drive File instance.
 * @return String The file's content if successful, null otherwise.
 */
function downloadFile($service, $file) {
  $downloadUrl = $file->getDownloadUrl();
  if ($downloadUrl) {
    $request = new apiHttpRequest($downloadUrl, 'GET', null, null);
    $httpRequest = $service->getIo()->authenticatedRequest($request);
    if ($httpRequest->getResponseHttpCode() == 200) {
      return $httpRequest->getResponseBody();
    } else {
      // An error occurred.
      return null;
    }
  } else {
    // The file doesn't have any content stored on Drive.
    return null;
  }
}

/**
 * Rename a file.
 *
 * @param apiDriveService $service Drive API service instance.
 * @param string $fileId ID of the file to rename.
 * @param string $newTitle New title for the file.
 * @return DriveFile The updated file. NULL is returned if an API error occurred.
 */
function renameFile($service, $fileId, $newTitle) {
  try {
    $file = new  Google_Service_Drive_DriveFile();
    $file->setTitle($newTitle);

    $updatedFile = $service->files->patch($fileId, $file, array(
      'fields' => 'title'
    ));

    return $updatedFile;
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
}

?>