#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
rm -rf /var/www
ln -fs /vagrant /var/www
apt-get -y install emacs
apt-get -y install libapache2-mod-php5
a2enmod php5
apt-get install -y curl libcurl3 libcurl3-dev php5-curl
apt-get -y install php5-mcrypt
cp /vagrant/php.ini /etc/php5/apache2/php.ini

service apache2 restart
