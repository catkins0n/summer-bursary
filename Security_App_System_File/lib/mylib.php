
<?php
	
	/**
	 * Returns the string specified by the key in the .json file specified
	 *
	 * @param string $key
	 *  	Key which the element is specified by
	 * 
	 * @param string $path
	 *  	Path to the .json file
	 * 
	 * @return string
     *    Specified Element in the .json file
	 */
	function getJSONField($key, $path){
		$contents = file_get_contents($path);
		$json = json_decode($contents , true);
		return $json[$key];
	}
	
?>