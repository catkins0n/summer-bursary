This folder needs the Dropbox API installed into it.
When done as the file-paths in the scripts expect, you should have a subdirectory structure that includes, e.g.

./dropbox-sdk/Dropbox/AppInfo.php
...

(should just be a matter of unZIPing what you get from Dropbox)

get-sd.php contains the method that (includes a crypto key (!!)) and does the Dropbox / NeSI DataFabric proxying

app-info.json needs the specified contents filled in.

I think I used mcrypt-*.php on the command-line for building tests.
