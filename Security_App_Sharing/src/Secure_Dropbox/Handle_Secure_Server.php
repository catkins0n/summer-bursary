<?php

require_once __DIR__.'/../../lib/Google/autoload.php';
require_once __DIR__.'/Handle_MySQL.php';

/**
 * Manipulates a Google Drive library ("secure" library). If the client is not logged into Google
 * or the client's access token has expired (after ~60 minutes) the browser is redirected to '/Authorize_Google.php' 
 * If redirected, after the access token is set, the browser is redirected to '/web-file-browser.php' and the request
 * needs to be recarried out by the user.
 * 
 * IMPORTANT: during set up, read the 'IMPORTANT' field in the '/Authorize_Google.php' file found in the README.md file.
 * 
 * @author Craig Atkinson
 * @date 05/12/2014
 */
class Handle_Secure_Server{

	/**
	 * Handles the MySQL Database
	 */
	private $hMySQL;
	
	/**
	 * Constructs the Class by initiating $hMySQL
	 */
	 public function __construct(){
	 	global $hMySQL;
	 	$hMySQL = new Handle_MySQL();
	 }

	/**
	 * Uploads the file at $_FILES['file']['name'] to the Secure Google Drive, returning the file's ID given by Google Drive
	 * to specify the file. 
	 *
	 * @param string $filePath
	 * 		the path of the file being uploaded in Dropbox
	 * 
	 * @return string
	 * 		representing the ID of the file which has been uploaded to Google Drive
	 */
	public function uploadFile($filePath){
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		
	 	$file = new Google_Service_Drive_DriveFile();
		$file->title = $_FILES['file']['name'];
		$file->properties = array($this->makeProperty("Outsourced_to_Dropbox", "true", "PRIVATE"));
		$chunkSizeBytes = 1 * 1024 * 1024;

		// Call the API with the media upload, defer so it doesn't immediately return.
		$client->setDefer(true);
		
		try{
			$request = $service->files->insert($file);
		}catch(Google_Auth_Exception $e){
			unset($_SESSION['access_token']);
			$this->uploadFile();
			return null;
		}
		// Create a media file upload to represent our upload process.
		$media = new Google_Http_MediaFileUpload(
  			$client,
	  		$request,
  			'text/plain',
  			null,
	  		true,
  			$chunkSizeBytes
		);
		$media->setFileSize(filesize($_FILES['file']['tmp_name']));

		// Upload the various chunks. $status will be false until the process is
		// complete.
		$status = false;
		$handle = fopen($_FILES['file']['tmp_name'], "rb");
		while (!$status && !feof($handle)) {
		  	$chunk = fread($handle, $chunkSizeBytes);
  			$status = $media->nextChunk($chunk);
		}

		// The final value of $status will be the data from the API for the object
		// that has been uploaded.
		$result = false;
		if($status != false) {
  			$result = $status;
		}

		fclose($handle);
		// Reset to the client to execute requests immediately in the future.
		$client->setDefer(false);
		
		//update MySQL database
		global $hMySQL;
		$hMySQL->storeFile($this->getID(), $result->id, $filePath);
		return $result->id;
	}
	
	/**
	 * Permanently deletes the file from the Secure Server
	 *
	 * @param string $path
	 * 		the path specifying the file which is being removed from the Secure Server
	 */
	public function delete($path){
		global $hMySQL;
		$fileID = $hMySQL->getFileID($this->getID(), $path);
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		$service->files->delete($fileID);
		
		//update MySQL database
		$hMySQL->removeFile($this->getID(), $fileID);	
		$hMySQL->removeAllSharedFile($fileID);
	}
	
	/**
 	 * Returns a Google_Service_Drive_Property which resembles the input parameters
 	 *
	 * @param string $key
	 * 		ID of the property.
	 * @param string $value 
	 * 		property value.
	 * @param string $visibility
	 * 		'PUBLIC' to make the property visible by all apps, or 'PRIVATE' to make it only
	 * 		available to the app that created it.
	 * 
	 * @return Google_Service_Drive_Property
	 * 		which resembles the input parameters
	 */
	private function makeProperty($key, $value, $visibility){
		$newProperty = new Google_Service_Drive_Property();
	  	$newProperty->setKey($key);
  		$newProperty->setValue($value);
	  	$newProperty->setVisibility($visibility);
  		return $newProperty;
	}
	
	/**
	 * Returns the $file's property/value with the specified $key, 
	 * null is returned if the $key is not found
	 * 
	 * @param Google_Service_Drive_DriveFile $file
	 * 		file which the property is being extracted from
	 * @param string $key
	 * 		key which specifies the property being returned
	 *
	 * @return string
	 * 		Contents of the $file's property with the specified $key, 
	 * 		null is returned if the $key is not found
	 */
	private function getProperty($file, $key){
	foreach ($file->properties as $tmp){
		if($tmp['key'] === $key)
			return $tmp['value'];
	}
		return null;
	}
	
	/**
	 * Returns the Google_Client authenticated with $_SESSION['access_token']. If its not set, 
	 * The browser is redirected to '/Authorize_Google.php' and then redirected to the
	 * '/web-file-browser.php', to manually redo the request.
	 * 
	 * @return Google_Client
	 * 		which is authenticated (by Google) or null if none is set
	 */
	 private function getClient(){
	 	if (!isset($_SESSION['access_token'])) {
			header("Location: " . $this->getAuthURL());
		}
		$client = new Google_Client();
		$client->setAccessToken($_SESSION['access_token']);
	 	return $client;
	 }
	
	/**
	 * Returns the Client's ID number (assigned by Google)
	 *
	 * @return string
	 * 		the Client's ID number (assigned by Google)
	 */
	public function getID(){
		$client = $this->getClient();
		$plus = new Google_Service_Oauth2($client);
		$userinfo = $plus->userinfo;
		return $userinfo->get()->getId();
	}
	
	/**
	 * Returns the Client's email (assigned to the Google account)
	 *
	 * @return string
	 * 		the Client's email (assigned to the Google account)
	 */
	public function getEmail(){
		$client = $this->getClient();
		$plus = new Google_Service_Oauth2($client);
		$userinfo = $plus->userinfo;
		return $userinfo->get()->getEmail();
	}
	
	/**
	 * Returns the Metadata for the file specified by the file's $path
	 * 
	 * @param string $path
	 * 		path which the file is represented in Dropbox
     * @param bool $isShared
     * 		true if the file is shared i.e. not owned by this user, false otherwise.
	 * 
	 * @return string 
	 * 		representing the specified file's Metadata
	 */
	public function getMetadata($path, $isShared){
		global $hMySQL;
		if($isShared)
			$fileID = $hMySQL->getSharedFileID($this->getEmail(), $path);
		else
			$fileID = $hMySQL->getFileID($this->getID(), $path);
			
		if($isShared){
			echo "<header><h1>ERROR:</h1></header> Sorry shared files cannot be retrieved because at the conclusion of my project
					I could not find a google drive api request (php) that returned the shared file. 
					This seems to be a problem other people are having as well, so it will hopefully be
					avaliable in the future.
					<br><br>echoed from: getMetadata() in http://127.0.0.1:4567/src/Secure_Dropbox/Handle_Secure_Server.php";
			return NULL;			
		}
			
	 	$file = $this->getFile($fileID);
	 	if($file == null) return null;
    	return '<pre>'. print_r($file, true). '</pre>';
	 }
	 
	 /**
	  * Returns the download link to the file specified by the file's $path
	  * 
	  * @param string $path
	  * 	path which the file is represented in Dropbox
      * @param bool $isShared
 	  * 	true if the file is shared i.e. not owned by this user, false otherwise.
	  * 
	  * @return string
	  * 	download link to the file specified by the file's $path
	  */
	 public function getFileDownloadLink($path, $isShared){
		global $hMySQL;
		if($isShared)
			$fileID = $hMySQL->getSharedFileID($this->getEmail(), $path);
		else
			$fileID = $hMySQL->getFileID($this->getID(), $path);
			
		if($isShared){
			echo "<header><h1>ERROR:</h1></header> Sorry shared files cannot be retrieved because at the conclusion of my project
					I could not find a google drive api request (php) that returned the shared file. 
					This seems to be a problem other people are having as well, so it will hopefully be
					avaliable in the future.
					<br><br>echoed from: getFileDownloadLink() in http://127.0.0.1:4567/src/Secure_Dropbox/Handle_Secure_Server.php";
			return NULL;			
		}
			
		$file = $this->getFile($fileID);
	 	return $file->getWebContentLink();
	 }
	
	/**
	 * Returns the Google Drive File specified by the $fileID, null is returned if an error occurs
	 * 
	 * @param string $fileID
	 * 		ID specifying the file in the Google Drive library
	 * 
	 * @return Google_Service_Drive_DriveFile
	 * 		specified by the $fileID, null is returned if an error occurs
	 */
	 private function getFile($fileID){
	 	$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		try{
			return $service->files->get($fileID);
		}catch(Google_Auth_Exception $e){
			unset($_SESSION['access_token']);
			header("Location: " . $this->getAuthURL());
		}
	 }
	
	/**
	 * Returns true if the access token has expired and false if it is still valid.
	 * 
	 * @return bool
	 * 		true if the access token has expired and false if it is still valid
	 */
	public function isAccessTokenExpired(){
	 	return $this->getClient()->isAccessTokenExpired();
	}
	
	/**
	 * Returns a string specifying the URL path on the server to the file which authorizes 
	 * the app's access to the secure server
	 *
	 * @return string 
	 * 		specifying the URL path on the server to the file which authorizes 
	 * 		the app's access to the secure server
	 */
	 public function getAuthURL(){
	 	return "http://127.0.0.1:4567/src/Secure_Dropbox/Authorize_Google.php";
	 }
	 
	/**
	 * Returns all the file's paths associated with the Client
	 *
	 * @return array
	 * 		all the file's paths associated with the Client
	 */
	public function getFilesPaths(){
		global $hMySQL;
		$tmp = $hMySQL->getFilesPaths($this->getID());
		$result = array();
		foreach ($tmp as $path) {
			array_push($result, $path);
		}
		
		return $result;
	}
	
	/**
	 * Returns true if the file specified by the $path exists 
	 * (as one of the user's files i.e. not shared to the user), false otherwise
	 * 
	 * @param string $path 
	 * 		the path which is being queried
	 * 
	 * @return bool
	 * 		which is true if the file specified by the $path exists 
	 * 		(as one of the user's files i.e. not shared to the user), false otherwise
	 */
	public function containsFile($path){
		return in_array($path, $this->getFilesPaths());
	}
	
	/**
	 * Inserts a permission so that the shared file (specified by its path) can be accessed by the user specified.
	 * When an error occurs a message is displayed.
	 * 
	 * @param string $filePath
	 * 		the path to the shared file which is having the permission inserted
	 * @param string $email
	 * 		email of the user which is having the permission granted
	 * @param string $role
	 * 		'reader' to allow the user to read the file or
	 * 		'writer' to allow the user to write to the file 
	 */
	public function insertPermission($filePath, $email, $role){
		global $hMySQL;
		$fileID = $hMySQL->getFileID($this->getID(), $filePath);
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		
		//check that we aren't downdowngrading a permission
		if($role == 'reader'){
			$tmpID = $service->permissions->getIdForEmail($email);
			$permissionID = $tmpID['id'];
			try{
				$tmp = $service->permissions->get($fileID, $permissionID);
			}catch (Google_Service_Exception $e){
				$tmp = NULL;
			}
			if($tmp != NULL && 'writer' == $tmp->role){
				echo "<header><h1>ERROR: Permission cannot be downgraded!". "</h1></header>";
				return;
			}
		}
		
		$permission = new Google_Service_Drive_Permission();
		$permission->emailAddress = $email;
		$permission->setType('user');
		$permission->setValue($email);
		$permission->setRole($role);
		try{
			$service->permissions->insert($fileID, $permission);
		}catch (Google_Service_Exception $e){
			echo "<header><h1>ERROR: Permission could not be granted to $email" . "</h1></header>";
			return;
		}
		if($hMySQL->getSharedPermission($email, $fileID) == NULL)
			$hMySQL->storeSharedFile($email, $fileID, $role, $filePath);
		else
			$hMySQL->updateSharedFilePermission($email, $fileID, $role);
	}
	
	/**
	 * Deletes a permission so that the shared file (specified by its path) can no longer be accessed
	 * by the user specified. When an error occurs a message is displayed.
	 * 
	 * @param string $filePath
	 * 		the path to the shared file which is having the permission deleted
	 * @param string $email
	 * 		email of the user which is having the permission deleted
	 */
	public function deletePermission($filePath, $email){
		global $hMySQL;
		$fileID = $hMySQL->getFileID($this->getID(), $filePath);
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		$permissionID = $service->permissions->getIdForEmail($email)->getId();	
		try{	
			$service->permissions->delete($fileID, $permissionID);
		}catch (Google_Service_Exception $e){
			echo "<header><h1>ERROR: Permission could not be revoked from $email" . "</h1></header>";
			return;
		}
		$hMySQL->removeSharedFile($email, $fileID);
	}
	
	/**
	 * Returns a table representing all permissions for the shared file specified by its $filePath
	 * 
	 * @param string $filePath
	 * 		the path to the shared file which is having its permissions returned
	 *
	 * @return string
	 * 		table representing all permissions for the shared file specified by its $filePath
	 */
	public function getPermissionTable($filePath){
		global $hMySQL;		
		$fileID = $hMySQL->getFileID($this->getID(), $filePath);
		$client = $this->getClient();
		$service = new Google_Service_Drive($client);
		$tmp = $service->permissions->listPermissions($fileID);
		$permissions = $tmp['modelData']['items'];
		$result = "";
		$result .= "<table>";
		$result .= "<tr>";
		$result .= "<td><u>Name:</u></td>";
		$result .= "<td><u>Email Address:</u></td>";
		$result .= "<td><u>Permission:</u></td>";
		$result .= "</tr>";
		foreach($permissions as $user){
			$result .= "<tr>";
			if(isset($user['name']))
				$result .= "<td>".($user['name'] == ""? "unknown":$user['name'])."</td>";
			else
				$result .= "<td></td>";
			if(isset($user['emailAddress']))
				$result .= "<td>".$user['emailAddress']."</td>";
			else
				$result .= "<td></td>";
			if(isset($user['role']))
				$result .= "<td>".$user['role']."</td>";
			else
				$result .= "<td></td>";
			$result .= "</tr>";
		}
		$result .= "</table>";
		
		return $result;
	}
	
	/**
	 * Returns the user's permission to the shared file at the specified $filePath
	 * 
	 * @param string $filePath
	 * 		the path which the shared file is at
	 *
	 * @return string
	 * 		permission the user has to the shared file. Values are:
	 * 			- 'reader' to allow the user to read the file
	 * 			- 'writer' to allow the user to write to the file 
	 */
	 public function getPermission($filePath){
	 	$email = $this->getEmail();
		global $hMySQL;		
		$fileID = $hMySQL->getSharedFileID($email, $filePath);
		return $hMySQL->getSharedPermission($email, $fileID);
	 }
	 
	/**
	 * Returns all the shared file's paths associated with the Client
	 *
	 * @return array
	 * 		all the shared file's paths associated with the Client
	 */
	public function getSharedFilesPaths(){
		global $hMySQL;
		$tmp = $hMySQL->getSharedFilesPaths($this->getEmail());
		$result = array();
		foreach ($tmp as $path) {
			array_push($result, $path);
		}
		
		return $result;
	}
}	

?>