<?php

require_once __DIR__.'/../../lib/Google/autoload.php';
require_once('../mysqli_connect.php');


/**
 * Manipulates MySQL Database. This database holds account's/project's googleIDs, fileIDs and the paths the files should be
 * presented at. It also holds the email addresses, fileIDs and paths of files which users have shared with others using this App.
 *
 * @author Craig Atkinson
 * @date 13/02/2015
 */
class Handle_MySQL{
	
	/**
	 * Returns all the file's paths associated with the $googleID. These paths are stored under their fileID
	 * in an associative array.
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account which is having its file paths returned
	 *
	 * @return array
	 * 		all the file's paths associated with the $googleID. These paths are stored under their fileID
	 * 		in the associative array
	 */
	public function getFilesPaths($googleID){
		$keys = array();
		$result = array();
		$dbc = getDatabaseConnection();

		$query = "SELECT fileID FROM secureDatabaseInfo.accountFiles WHERE googleID = '$googleID'";
		$response = @mysqli_query($dbc, $query);
		if($response){
			while($row = mysqli_fetch_array($response)){
				array_push($keys, $row['fileID']);
			}
		}
		mysqli_close($dbc);
		
		foreach($keys as $fileID){
			$result[$fileID] = $this->getPath($fileID);
		}
		
		return $result;
	}
	
	/**
	 * Returns the account's/project's fileID for the file which has the specified $path
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account which is having its file's ID returned
	 * @param string $path
	 * 		the path of the file whose fileID is being returned
	 * 
	 * @return string
	 * 		the account's/project's fileID for the file which has the specified $path
	 */
	public function getFileID($googleID, $path){
		$filesPaths = $this->getFilesPaths($googleID);
		return array_search($path, $filesPaths);
	}
	
	/**
	 * Stores file information to the database. True is returned if the operation was successful and 
	 * false otherwise
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account that the file is being stored under
	 * @param string $fileID
	 * 		the fileID (given by google) for the file which is being stored
	 * @param string $path
	 * 		the path which the file should be presented at (on dropbox)
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function storeFile($googleID, $fileID, $path){
        $success1 = $this->insertAccountFile($googleID, $fileID);
        $success2 = $this->insertFilesPath($fileID, $path);
        return $success1 && $success2;
	}
	
	/**
	 * Removes the files information from the database. True is returned if the operation was successful and 
	 * false otherwise
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account that the file is being deleted from
	 * @param string $fileID
	 * 		the fileID of the file being removed
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function removeFile($googleID, $fileID){
		$success1 = $this->removeAccountFile($googleID, $fileID);
		$success2 = $this->removeFilesPath($fileID);
        return $success1 && $success2;
	}
	
	/**
	 * Updates/changes the path of a file (specified by its current path). 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account that the file is being updated in
	 * @param string $oldPath
	 * 		the original/current path of the file being updated to the $newPath
	 * @param string $newPath
	 * 		the path which the file is being updated to have
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function updateFilesPath($googleID, $oldPath, $newPath){
		$dbc = getDatabaseConnection();
		$fileID = $this->getFileID($googleID, $oldPath);
		
		$sql = "UPDATE secureDatabaseInfo.filesPaths SET path = '$newPath' WHERE fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Inserts into the database a $fileID to be stored under the $googleID of the project/account. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account that the fileID is being inserted under
	 * @param string $fileID
	 * 		the fileID for the file (given by google), which is being inserted under the $googleID
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function insertAccountFile($googleID, $fileID){
		$dbc = getDatabaseConnection();
		
		$query = "INSERT INTO secureDatabaseInfo.accountFiles (googleID, fileID) VALUES (?, ?)";
		$stmt = mysqli_prepare($dbc, $query);
		mysqli_stmt_bind_param($stmt, "ss", $googleID, $fileID);
		mysqli_stmt_execute($stmt);
		$affected_rows = mysqli_stmt_affected_rows($stmt);
		mysqli_stmt_close($stmt);   
		
		mysqli_close($dbc);
		return $affected_rows == 1;
	}
	
	/**
	 * Inserts into the database a $fileID with its $path. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID which is being inserted with its $path
	 * @param string $path
	 * 		the path which the file should be represented in Dropbox.
	 * 		This is the path which is being inserted under the specified $fileID
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function insertFilesPath($fileID, $path){
		$dbc = getDatabaseConnection();
		
		$query = "INSERT INTO secureDatabaseInfo.filesPaths (fileID, path) VALUES (?, ?)";
		$stmt = mysqli_prepare($dbc, $query);
		mysqli_stmt_bind_param($stmt, "ss", $fileID, $path);
		mysqli_stmt_execute($stmt);
		$affected_rows = mysqli_stmt_affected_rows($stmt);
		mysqli_stmt_close($stmt);
		
		mysqli_close($dbc);
		return $affected_rows == 1;
	}
	
	/**
	 * Removes the $fileID stored under the $googleID from the database.
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $googleID
	 * 		the googleID for the project/account that the fileID is being removed from
	 * @param string $fileID
	 * 		the fileID for the file (given by google), which is being removed from the $googleID's specified files
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function removeAccountFile($googleID, $fileID){
		$dbc = getDatabaseConnection();
		
		$sql = "DELETE FROM secureDatabaseInfo.accountFiles WHERE googleID = '$googleID' AND fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Removes from the database the $fileID with its path. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID which is being removed with its path
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function removeFilesPath($fileID){
		$dbc = getDatabaseConnection();
		
		$sql = "DELETE FROM secureDatabaseInfo.filesPaths WHERE fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Returns the path which is stored under the $fileID in the database.
	 *
	 * @param string $fileID
	 * 		the fileID for the file, which is having its stored path returned
	 *
	 * @return string
	 * 		the path which is stored under the $fileID in the database
	 */
	private function getPath($fileID){
		$result = NULL;
		$dbc = getDatabaseConnection();
		
		$query = "SELECT path FROM secureDatabaseInfo.filesPaths WHERE fileID = '$fileID'";
		$response = @mysqli_query($dbc, $query);
		if($response){
			$result = mysqli_fetch_array($response);
		}
		mysqli_close($dbc);
		
		return ($result == NULL? NULL:$result['path']);
	}
	
	/**
	 * Returns all the file's paths associated with the $email (in the sharing database). 
	 * These paths are stored under their fileID in an associative array.
	 * 
	 * @param string $email
	 * 		the email for the project/account which is having its shared file paths returned
	 *
	 * @return array
	 * 		all the shared file's paths associated with the $email. These paths are stored under their fileID
	 * 		in the associative array
	 */
	public function getSharedFilesPaths($email){
		$keys = array();
		$result = array();
		$dbc = getDatabaseConnection();

		$query = "SELECT fileID FROM secureDatabaseInfo.sharedAccountFiles WHERE email = '$email'";
		$response = @mysqli_query($dbc, $query);
		if($response){
			while($row = mysqli_fetch_array($response)){
				array_push($keys, $row['fileID']);
			}
		}
		mysqli_close($dbc);
		
		foreach($keys as $fileID){
			$result[$fileID] = $this->getSharedPath($fileID);
		}
		
		return $result;
	}
	
	/**
	 * Returns the account's/project's fileID for the file which has the specified $path (in the sharing database)
	 * 
	 * @param string $email
	 * 		the email for the project/account which is having its shared file's ID returned
	 * @param string $path
	 * 		the path of the shared file whose fileID is being returned
	 * 
	 * @return string
	 * 		the account's/project's fileID for the shared file which has the specified $path
	 */
	public function getSharedFileID($email, $path){
		$filesPaths = $this->getSharedFilesPaths($email);
		return array_search($path, $filesPaths);
	}
	
	/**
	 * Stores file information to the sharing database. True is returned if the operation was successful and 
	 * false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the shared file is being stored under
	 * @param string $fileID
	 * 		the fileID (given by google) for the shared file which is being stored
	 * @param string $permission
	 * 		the permission the user has been granted to the file. Values are:
	 * 			- 'reader' to allow the user to read the file
	 * 			- 'writer' to allow the user to write to the file 
	 * @param string $path
	 * 		the path which the shared file should be presented at (on dropbox)
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function storeSharedFile($email, $fileID, $permission, $path){
        $success1 = $this->insertSharedAccountFile($email, $fileID, $permission);
        $success2 = $this->insertSharedFilesPath($fileID, $path);
        return $success1 && $success2;
	}
	
	/**
	 * Removes the files information from the sharing database. True is returned if the operation was successful and 
	 * false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the shared file is being deleted from
	 * @param string $fileID
	 * 		the fileID of the shared file being removed
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function removeSharedFile($email, $fileID){
		$success1 = $this->removeSharedAccountFile($email, $fileID);
		$success2 = $this->removeSharedFilesPath($fileID);
        return $success1 && $success2;
	}
	
	/**
	 * Removes all of the files information from the sharing database for every user the file (specified by its $fileID) was shared with.
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID of the shared file being removed
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function removeAllSharedFile($fileID){
		$success1 = $this->removeAllSharedAccountFile($fileID);
		$success2 = $this->removeSharedFilesPath($fileID);
        return $success1 && $success2;
	}
	
	/**
	 * Updates/changes the path of a file (specified by its current path) in the sharing database. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the shared file is being updated in
	 * @param string $oldPath
	 * 		the original/current path of the shared file being updated to the $newPath
	 * @param string $newPath
	 * 		the path which the shared file is being updated to have
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function updateSharedFilesPath($email, $oldPath, $newPath){
		$dbc = getDatabaseConnection();
		$fileID = $this->getSharedFileID($email, $oldPath);
		
		$sql = "UPDATE secureDatabaseInfo.SharedFilesPaths SET path = '$newPath' WHERE fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Updates/changes the user's permission to the shared file specified by its $fileID.
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the shared file's permission is being updated in
	 * @param string $fileID
	 * 		the fileID of the shared file which is having its permission changed
	 * @param string $permission
	 * 		the permission the user will have to the shared file
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	public function updateSharedFilePermission($email, $fileID, $permission){
		$dbc = getDatabaseConnection();
		
		$sql = "UPDATE secureDatabaseInfo.sharedAccountFiles SET permission = '$permission' WHERE  email = '$email' AND fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Inserts into the sharing database a $fileID to be stored under the $email of the project/account. 
	 * This entry is also associated with a $permission. True is returned if the operation was 
	 * successful and false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the fileID is being inserted under
	 * @param string $fileID
	 * 		the fileID for the shared file (given by google), which is being inserted under the $email
	 * @param string $permission
	 * 		the permission the user has been granted to the file. Values are:
	 * 			- 'reader' to allow the user to read the file
	 * 			- 'writer' to allow the user to write to the file 
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function insertSharedAccountFile($email, $fileID, $permission){
		$dbc = getDatabaseConnection();
		
		$query = "INSERT INTO secureDatabaseInfo.sharedAccountFiles (email, fileID, permission) VALUES (?, ? , ?)";
		$stmt = mysqli_prepare($dbc, $query);
		mysqli_stmt_bind_param($stmt, "sss", $email, $fileID, $permission);
		mysqli_stmt_execute($stmt);
		$affected_rows = mysqli_stmt_affected_rows($stmt);
		mysqli_stmt_close($stmt);   
		
		mysqli_close($dbc);
		return $affected_rows == 1;
	}
	
	/**
	 * Inserts into the sharing database a $fileID with its $path. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID which is being inserted with its $path
	 * @param string $path
	 * 		the path which the shared file should be represented in Dropbox.
	 * 		This is the path which is being inserted under the specified $fileID
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function insertSharedFilesPath($fileID, $path){
		$dbc = getDatabaseConnection();
		
		$query = "INSERT INTO secureDatabaseInfo.sharedFilesPaths (fileID, path) VALUES (?, ?)";
		$stmt = mysqli_prepare($dbc, $query);
		mysqli_stmt_bind_param($stmt, "ss", $fileID, $path);
		mysqli_stmt_execute($stmt);
		$affected_rows = mysqli_stmt_affected_rows($stmt);
		mysqli_stmt_close($stmt);
		
		mysqli_close($dbc);
		return $affected_rows == 1;
	}
	
	/**
	 * Removes the $fileID stored under the $email from the sharing database.
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $email
	 * 		the email for the project/account that the fileID is being removed from
	 * @param string $fileID
	 * 		the fileID for the shared file (given by google), which is being removed from the 
	 * 		$email's specified shared files
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function removeSharedAccountFile($email, $fileID){
		$dbc = getDatabaseConnection();
		
		$sql = "DELETE FROM secureDatabaseInfo.sharedAccountFiles WHERE email = '$email' AND fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Removes the file (specified by its $fileID) from all user's accounts in the sharing database.
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID for the shared file (given by google), which is being removed from all users
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function removeAllSharedAccountFile($fileID){
		$dbc = getDatabaseConnection();
		
		$sql = "DELETE FROM secureDatabaseInfo.sharedAccountFiles WHERE fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}
	
	/**
	 * Removes from the sharing database the $fileID with its path. 
	 * True is returned if the operation was successful and false otherwise
	 * 
	 * @param string $fileID
	 * 		the fileID which is being removed with its path
	 * 
	 * @return bool
	 * 		true is returned if the operation was successful and 
	 * 		false otherwise
	 */
	private function removeSharedFilesPath($fileID){
		$dbc = getDatabaseConnection();
		
		$sql = "DELETE FROM secureDatabaseInfo.sharedFilesPaths WHERE fileID = '$fileID'";
		$success = $dbc->query($sql);
		
		mysqli_close($dbc);
		return $success;
	}	
	
	/**
	 * Returns the path which is stored under the $fileID in the sharedFilesPaths database.
	 *
	 * @param string $fileID
	 * 		the fileID for the shared file, which is having its stored path returned
	 *
	 * @return string
	 * 		the path which is stored under the $fileID in the sharedFilesPaths database
	 */
	private function getSharedPath($fileID){
		$result = NULL;
		$dbc = getDatabaseConnection();
		
		$query = "SELECT path FROM secureDatabaseInfo.sharedFilesPaths WHERE fileID = '$fileID'";
		$response = @mysqli_query($dbc, $query);
		if($response){
			$result = mysqli_fetch_array($response);
		}
		mysqli_close($dbc);
		
		return ($result == NULL? NULL:$result['path']);
	}
	
	/**
	 * Returns the permission which the user (specified by $email) has with the shared file (specified by $fileID).
	 *
	 * @param string $email
	 * 		the email of the user
	 * @param string $fileID
	 * 		the fileID for the shared file, which is having its stored permission returned
	 *
	 * @return string
	 * 		permission value stored in the sharing database:
	 * 			- 'reader' to allow the user to read the file or
	 * 			- 'writer' to allow the user to write to the file 
	 */
	public function getSharedPermission($email, $fileID){
		$result = NULL;
		$dbc = getDatabaseConnection();
		
		$query = "SELECT permission FROM secureDatabaseInfo.sharedAccountFiles WHERE email = '$email' AND fileID = '$fileID'";
		$response = @mysqli_query($dbc, $query);
		if($response){
			$result = mysqli_fetch_array($response);
		}
		mysqli_close($dbc);
		
		return ($result == NULL? NULL:$result['permission']);
	}

}	

?>