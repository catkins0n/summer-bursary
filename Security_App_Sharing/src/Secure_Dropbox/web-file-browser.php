<?php

require_once __DIR__.'/../../lib/Dropbox/strict.php';

$appInfoFile = __DIR__."/web-file-browser.app";

// NOTE: You should be using Composer's global autoloader.  But just so these examples
// work for people who don't have Composer, we'll use the library's "autoload.php".
require_once __DIR__.'/../../lib/Dropbox/autoload.php';

require_once __DIR__.'/Handle_Secure_Server.php';		//added

use \Dropbox as dbx;

/**
 * Displays a file browser allowing the user to manipulate the client's Dropbox in the web browser. 
 * This also includes uploading files "securely" by storing them in the client's
 * Google Drive library and the file ID to access that file is stored in a MySQL database. This
 * App also allows users to specify which "secure" files they would like to share with other users
 * specified by their account's email. Please note: "secure" is used in the comments as this is a 
 * prototype, as we are pretending Google Drive is a secure server.
 * 
 * Adapted from dropbox-sdk-php-1.1.4/examples/web-file-browser.php
 * 
 * @author Craig Atkinson
 * @date 05/12/2014
 */
session_start();

$requestPath = init();
DEFINE ('LOGOUT_DROPBOX_TEXT', 'Logout of Dropbox');
DEFINE ('LOGOUT_SECURE_SERVER_TEXT', 'Logout of Google');

$hss = new Handle_Secure_Server();

/*
 * Unless carrying out authentication, it checks to see if the user is logged in without their access tokens being
 * expired. If they are logged out or expired the browser is appropriately redirected
 */
if($requestPath != "/dropbox-auth-start" && $requestPath != "/dropbox-auth-finish"){
	$dbxClient = getClient();
    
	//check that the user is logged into Dropbox
	if ($dbxClient === false) {
    	header("Location: ".getPath("dropbox-auth-start"));
	    exit;
	}

	//check that the user is logged into the "Secure" Server   
	if($hss->isAccessTokenExpired()){
   		unset($_SESSION['access_token']);
		header("Location: " . $hss->getAuthURL());
	}
}

/*
 * If the request path is "/", it renders a file or folder in the client's Dropbox or "secure" library
 */
if ($requestPath === "/") {
    $path = "/";
    if (isset($_GET['path'])) $path = $_GET['path'];
    
    $entry = $dbxClient->getMetadataWithChildren($path);
    $isSecurePath = false;
    if(isset($_GET['security']))
	    $isSecurePath = $_GET['security'];

	if($isSecurePath || $entry == null){
		if (!isFilePath($path)){
    	    echo renderFolder($path, null);
	    }
    	else {
    		$isShared = false;
    		if(isset($_GET['shared']))
	    		$isShared = $_GET['shared'];
    		echoReturnToParentLink($path);
    		echo renderSecureFile($path, $isShared);
	    }
    }else{
    	if ($entry['is_dir']) {
    	  	echo renderFolder($entry['path'], $entry['contents']);
		}
    	else {
			echoReturnToParentLink($path);
    		echo renderNormalFile($entry);
    	}
   	}
}

/*
 * Deletes the file specified by the path URL parameter 
 */
else if ($requestPath == "/delete") {
	$path = $_GET['path'];
	echo "Deleted: ".$path;
	echoReturnToParentLink($path);
	
	$entry = $dbxClient->getMetadataWithChildren($path);
	$isSecurePath = false;
    if(isset($_GET['security']))
	    $isSecurePath = $_GET['security'];

	if($isSecurePath || $entry == null){
		$hss->delete($path);
	}else{
		$dbxClient->delete($path);
	}
}

/*
 * Downloads/displays the Dropbox file specified by the path URL parameter
 */
else if ($requestPath == "/download") {

    if (!isset($_GET['path'])) {
        header("Location: ".getPath(""));
        exit;
    }
    $path = $_GET['path'];

    $fd = tmpfile();
    $metadata = $dbxClient->getFile($path, $fd);

    header("Content-Type: $metadata[mime_type]");
    fseek($fd, 0);
    fpassthru($fd);
    fclose($fd);
}

/*
 * The file specified by the user is uploaded to either Dropbox or the "secure" Google Drive library 
 * The file is saved to the "secure" library if the "$_POST['secure']" is set. Files are not saved if they
 *  have the same name as a file in the same directory on the server they are being uploaded to (Dropbox/Google Drive)
 */
else if ($requestPath === "/upload") {
    if (empty($_FILES['file']['name'])) {
        echo renderHtmlPage("Error", "Please choose a file to upload");
        echoReturnToParentLink("/");
        exit;
    }

    if (!empty($_FILES['file']['error'])) {
        echo renderHtmlPage("Error", "Error ".$_FILES['file']['error']." uploading file.  See <a href='http://php.net/manual/en/features.file-upload.errors.php'>the docs</a> for details");
        echoReturnToParentLink("/");
        exit;
    }

	$remoteDir = "/";
	if (isset($_POST['folder'])) 
		$remoteDir = $_POST['folder'];
	$remotePath = rtrim($remoteDir, "/")."/".$_FILES['file']['name'];
	
	$dbxMetadata = $dbxClient->getMetadataWithChildren($remotePath);
		
	if((isset($_POST['secure']) && $hss->containsFile($remotePath)) || 
		(!isset($_POST['secure']) && $dbxMetadata != null)){
					
		fileNameTakenError($remotePath);	
			
	}else{
		if(!isset($_POST['secure'])){
	    	$fp = fopen($_FILES['file']['tmp_name'], "rb");
	    	$result = $dbxClient->uploadFile($remotePath, dbx\WriteMode::add(), $fp);
   			fclose($fp);
	    	$str = print_r($result, true);
    	    echoReturnToParentLink($remotePath);
    		echo renderHtmlPage("Uploading File", "Result: <pre>$str</pre>");
    	}else{
			$hss->uploadFile($remotePath);
    		echoReturnToParentLink($remotePath);
    		echo renderHtmlPage("Uploaded File", $remotePath);
    	}
    }
}

/*
 * Logs the user out of either their Dropbox or Google account
 */
else if ($requestPath === "/account") {
	if(isset($_POST['logoutDropbox'])){
		unset($_SESSION['access-token']);
		header("Location: ".getPath("dropbox-auth-start"));
        exit;
	}else if(isset($_POST['logoutSecureServer'])){
		unset($_SESSION['access_token']);
		header("Location: " . $hss->getAuthURL());
	}
}

/*
 * Creates or deletes the folder specified. When deleting, every Drobpox and Google Drive file (owned) is deleted
 * from the library
 */
else if ($requestPath === "/folder") {
	$path = "/";
    if (isset($_POST['path'])) $path = $_POST['path'];
	//create folder
	if(isset($_POST['folderName']) && isset($_POST['submit']) && $_POST['submit'] == "create" && $_POST['folderName'] != ""){
		
		if(isValidFolderName($_POST['folderName'], $path)){
			getClient()->createFolder(($path === "/"? "":$path)."/".$_POST['folderName']);
			echo "Created";
			header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
		}else{
			echo "Error, folder could not be created as its name was either too long<br>
					and/or included illegal characters:<br>
					<pre>/ \ < > : \" | ? * .</pre>";
			echoReturnLink($path);
			echo "<br>".($path === "/"? "":$path)."/".$_POST['folderName'];
		}
		
    }
    //delete folder
    else if(isset($_POST['folderName']) && isset($_POST['submit']) && $_POST['submit'] == "delete" && $_POST['folderName'] != ""){
		$folderPath = ($path === "/"? "":$path)."/".$_POST['folderName'];
    	$entry = $dbxClient->getMetadataWithChildren($folderPath);
    	try{
    		if($entry != null && $entry['is_dir']){
    			$dbxClient->delete($folderPath);
    			removeSecureFilesInFolder($folderPath);
				header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
	    	}
    	}catch(dbx\Exception_BadResponseCode $e){
    		if(removeSecureFilesInFolder($folderPath))
				header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
    	}
    	echo "Error: Could not find folder to delete.";
		echoReturnLink($path);
    }else{
    	header("Location: " . getPath($path == "/"? "":"?path=".htmlspecialchars($path)));
    }
}

/*
 * Shows what permissions other users have been granted to the Google Drive file (at the path specified in the URL
 * parameter). Also allows permissions to be granted and revoked for users specified by their email addresses.
 */
else if ($requestPath === "/share") {
	$grantReadText = "grant_read";
	$grantWriteText = "grant_write";
	$revokeText = "revoke";
	$path = "/";
    if (isset($_GET['path'])) $path = $_GET['path'];
    
    if(isset($_GET['security']))
	    $isSecurePath = $_GET['security'];
	    
	if(isset($_POST['action']) && isset($_POST['email'])){
		$action = $_POST['action'];
		if($action == $grantReadText){
			$hss->insertPermission($path, $_POST['email'], "reader");
		}else if ($action == $grantWriteText){
			$hss->insertPermission($path, $_POST['email'], "writer");
		}else if($action == $revokeText){
			$email = $_POST['email'];
			$hss->deletePermission($path, $_POST['email']);
		}
	}
	
	echoReturnLinkWithParams($path);
	echo "<header><h2>Permissions for: " . $path . "</h2></header>";
	$table = $hss->getPermissionTable($path);
	echo $table;
	
    $sharePath = getPath("share?path=".htmlspecialchars($path)."&security=".$isSecurePath);
	$createFolderForm = <<<HTML
					<form action='$sharePath' method='post' enctype='multipart/form-data'>
			        <br><br>Email Address:
			        <input type="text" name="email">
			        <select name="action">
			        	<option value=$grantReadText>grant reading privileges</option>
			        	<option value=$grantWriteText>grant writing privileges</option>
			        	<option value=$revokeText>revoke privileges</option>
			        </select>
			        <input type='submit' name='submit'/>
			        </form>
HTML;
	echo $createFolderForm;
}

/*
 * Starts the Dropbox authentication process
 */
else if ($requestPath === "/dropbox-auth-start") {
    $authorizeUrl = getWebAuth()->start();
    header("Location: $authorizeUrl");
}

/*
 * Finishes the Dropbox authentication process, handling any errors
 */
else if ($requestPath === "/dropbox-auth-finish") {
    try {
        list($accessToken, $userId, $urlState) = getWebAuth()->finish($_GET);
        // We didn't pass in $urlState to finish, and we're assuming the session can't be
        // tampered with, so this should be null.
        assert($urlState === null);
    }
    catch (dbx\WebAuthException_BadRequest $ex) {
        respondWithError(400, "Bad Request");
        // Write full details to server error log.
        // IMPORTANT: Never show the $ex->getMessage() string to the user -- it could contain
        // sensitive information.
        error_log("/dropbox-auth-finish: bad request: " . $ex->getMessage());
        exit;
    }
    catch (dbx\WebAuthException_BadState $ex) {
        // Auth session expired.  Restart the auth process.
        header("Location: ".getPath("dropbox-auth-start"));
        exit;
    }
    catch (dbx\WebAuthException_Csrf $ex) {
        respondWithError(403, "Unauthorized", "CSRF mismatch");
        // Write full details to server error log.
        // IMPORTANT: Never show the $ex->getMessage() string to the user -- it contains
        // sensitive information that could be used to bypass the CSRF check.
        error_log("/dropbox-auth-finish: CSRF mismatch: " . $ex->getMessage());
        exit;
    }
    catch (dbx\WebAuthException_NotApproved $ex) {
        echo renderHtmlPage("Not Authorized?", "Why not?");
        exit;
    }
    catch (dbx\WebAuthException_Provider $ex) {
        error_log("/dropbox-auth-finish: unknown error: " . $ex->getMessage());
        respondWithError(500, "Internal Server Error");
        exit;
    }
    catch (dbx\Exception $ex) {
        error_log("/dropbox-auth-finish: error communicating with Dropbox API: " . $ex->getMessage());
        respondWithError(500, "Internal Server Error");
        exit;
    }

    // NOTE: A real web app would store the access token in a database.
    $_SESSION['access-token'] = $accessToken;

	header("Location: " . htmlspecialchars(getPath("")));
}

/*
 * Unlinks/logs out the Client from the App
 */
else if ($requestPath === "/dropbox-auth-unlink") {
    // "Forget" the access token.
    unset($_SESSION['access-token']);
    echo renderHtmlPage("Unlinked.",
        "Go back <a href='".htmlspecialchars(getPath(""))."'>home</a>.");
}

/*
 * For unknown request paths an error message is displayed
 */
else {
    echo renderHtmlPage("Bad URL", "No handler for $requestPath");
    exit;
}

/*
 * Renders the folder at the specified path, displaying the Dropbox contents and the files and folders specified as 
 * being in that directory by  MySQL database. Also displays a panel which allows the user to 
 * specify a file to upload and a checkbox for whether or not they want it to be uploaded "securely". Forms are 
 * also displayed for creating and removing folders and logging out of accounts.
 * 
 * @param string $path
 * 		representing the path to the folder being displayed in the browser
 * @param array $dbxContents
 * 		holding the contents of the folder in the Dropbox library (files & folders)
 */
function renderFolder($path, $dbxContents){
    // TODO: Add a token to counter CSRF attacks.
    $path = htmlspecialchars($path);
    
    $title = "Folder: $path";
    $uploadForm = getUploadForm($path);
    $folderForm = getFolderForm($path);
	$accountForm = getAccountForm($path);
	
	$displayed = array();//stores folders displayed already, so they are displayed once
	$listing = getDropboxFileAndFolders($dbxContents, $path, &$displayed);
	$secure_listing = getSecureFileAndFolders($path, &$displayed);
	$shared_listing = getSecureSharedFileAndFolders($path, &$displayed);

    return renderHtmlPage($title, $uploadForm.$listing.$secure_listing.$shared_listing.$folderForm.$accountForm);
}

/**
 * Returns the upload form which allows users to select a file and choose whether to upload it securely or not
 * 
 * @param string $path
 * 		path of the folder being displayed 
 *
 * @return string
 * 		upload form which allows users to select a file and choose whether to upload it securely or not
 */
function getUploadForm($path){
 	$upload_path = htmlspecialchars(getPath('upload'));
    return <<<HTML
        <form action='$upload_path' method='post' enctype='multipart/form-data'>
        <label for='file'>Upload file:</label> <input name='file' type='file'/>
        Secure <input type="checkbox" name="secure"/>
        <input type='submit' value='Upload'/>
        <input name='folder' type='hidden' value='$path'/>
        </form>
HTML;
}

/**
 * Returns the folder form which allows users to create and delete folders
 * 
 * @param string $path
 * 		path of the folder being displayed 
 *
 * @return string
 * 		folder form which allows users to create and delete folders
 */
function getFolderForm($path){
	$folderManagementPath = getPath('folder'."?path=".htmlspecialchars($path));
	return <<<HTML
					<form action='$folderManagementPath' method='post' enctype='multipart/form-data'>
			        <br><br>Folder:
			        <input type="text" name="folderName">
			        <input type='submit' name='submit' value='create'/>
			        <input type='submit' name='submit' value='delete' onclick="return confirm('Are you sure you want to delete this file?');"/>
			        <input name='path' type='hidden' value='$path'/>
			        </form>
HTML;
}

/**
 * Returns the account form which allows users to logout/change Dropbox and Google accounts
 * 
 * @param string $path
 * 		path of the folder being displayed 
 *
 * @return string
 * 		account form which allows users to logout/change Dropbox and Google accounts
 */
function getAccountForm($path){
	$logoutDropboxText = LOGOUT_DROPBOX_TEXT;
	$logoutSecureServerText = LOGOUT_SECURE_SERVER_TEXT;
    $accountManagementPath = getPath('account');
	return <<<HTML
				<form action='$accountManagementPath' method='post' enctype='multipart/form-data'>
    			<input type='submit' name='logoutDropbox' value='$logoutDropboxText'/>
    			<input type='submit' name='logoutSecureServer' value='$logoutSecureServerText'/>
    			</form>
HTML;
}

/**
 * Returns a list of hyperlinks which redirects to the user's Dropbox files and folders (in the current directory)
 * 
 * @param array $dbxContents
 * 		files and folders which are being displayed
 * @param string $path
 * 		path of the folder being displayed
 * @param array $displayed
 * 		names of folders which are included in the list (including any previously included ones)
 *
 * @return string
 * 		list of hyperlinks which redirects to the user's Dropbox files and folders (in the current directory)
 */
function getDropboxFileAndFolders($dbxContents, $path, $displayed){
	$parentPath = dirname($path);
	$listing = '______________________________<br>_______<u>DROPBOX FILES:</u>_______<br>';
	
	//go to parent directory hyperlink
	if($path != "/"){
		if($parentPath === "/")
	 	   $listing = "<div><a style='text-decoration: none' href='".getPath("")."'>Parent Directory..</a></div>";
		else
			$listing = "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($parentPath))."'>Parent Directory..</a></div>";
	}
	
    if($dbxContents != null){
    	foreach ($dbxContents as $child) {
        	$cp = $child['path'];
	        $cn = basename($cp);
    	    if ($child['is_dir']) 
    	    	$cn .= '/';
	        $cp = htmlspecialchars($cp);
		    $link = getPath("?path=".htmlspecialchars($cp));
    		$listing .= "<div><a style='text-decoration: none' href='$link'>$cn</a></div>";
    	    if($child['is_dir'] && !in_array($cn, $displayed)){
    	    	array_push($displayed, $cn);
    	   	}
    	}
    }
    return $listing;
}

/**
 * Returns a list of hyperlinks which redirects to the user's "secure" unshared/owned files and folders (in the current directory)
 * 
 * @param string $path
 * 		path of the folder being displayed
 * @param array $displayed
 * 		names of folders which are included in the list (including any previously included ones)
 *
 * @return string
 * 		list of hyperlinks which redirects to the user's "secure" unshared/owned files and folders (in the current directory)
 */
function getSecureFileAndFolders($path, $displayed){
	$secure_listing = '<br>______________________________<br>________<u>SECURE FILES:</u>________<br>';
    
    global $hss;
	$filesPaths = $hss->getFilesPaths();
	foreach ($filesPaths as $file){
	    if(isIncluded($path, $file)){
	  		$tmp = getNextInPath($path, $file);
	   		$link = getPath("?path=".htmlspecialchars($path.($path == "/"? "":"/").rtrim($tmp, "/"))."&security=".true);
	   		if(!in_array($tmp, $displayed)){
	   			if(!isFilePath($tmp))
		    		array_push($displayed, $tmp);
		    	$secure_listing .= "<div><a style='text-decoration: none' href='$link'>$tmp</a></div>"; 
	    	}
    	}
    }
    return $secure_listing;
}

/**
 * Returns a list of hyperlinks which redirects to the user's "secure" shared/not owned files and folders (in the current directory)
 * 
 * @param string $path
 * 		path of the folder being displayed
 * @param array $displayed
 * 		names of folders which are included in the list (including any previously included ones)
 *
 * @return string
 * 		list of hyperlinks which redirects to the user's "secure" shared/not owned files and folders (in the current directory)
 */
function getSecureSharedFileAndFolders($path, $displayed){
	$shared_listing = '<br>______________________________<br>____<u>SECURE SHARED FILES:</u>____<br>';
    
    global $hss;
	$filesPaths = $hss->getSharedFilesPaths();
	foreach ($filesPaths as $file){
	    if(isIncluded($path, $file)){
	  		$tmp = getNextInPath($path, $file);
	   		$link = getPath("?path=".htmlspecialchars($path.($path == "/"? "":"/").rtrim($tmp, "/"))."&security=".true."&shared=".true);
	   		if(!in_array($tmp, $displayed)){
	   			if(!isFilePath($tmp))
		    		array_push($displayed, $tmp);
		    	$shared_listing .= "<div><a style='text-decoration: none' href='$link'>$tmp</a></div>"; 
	    	}
    	}
    }
    return $shared_listing;
}

/**
 * Echos a return hyperlink to the page which when clicked returns the user's browser to the $path's parent directory
 * 
 * @param string $path
 * 		the path which the user's browser should redirect to its parent directory when the hyperlink is clicked
 */
function echoReturnToParentLink($path){
	$parent = dirname($path);
	if($parent === "/")
		echo "<div><a style='text-decoration: none' href='".getPath("")."'>Return to Dropbox</a></div>";
	else
		echo "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($parent))."'>Return to Dropbox</a></div>";
}

/**
 * Echos a return hyperlink to the page which when clicked returns the user's browser to the $path
 * 
 * @param string $path
 * 		the path which the user's browser should redirect to when the hyperlink is clicked
 */
function echoReturnLink($path){
	if($path === "/")
		echo "<div><a style='text-decoration: none' href='".getPath("")."'>Return to Dropbox</a></div>";
	else
		echo "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($path))."'>Return to Dropbox</a></div>";
}

/**
 * Echos a return hyperlink to the page which when clicked returns the user's browser to the $path.
 * This hyperlink includes the security and shared parameters in the URL (where nessessary).
 * 
 * @param string $path
 * 		the path which the user's browser should redirect to when the hyperlink is clicked
 */
function echoReturnLinkWithParams($path){
	$isSecurePath = false;
    if(isset($_GET['security']))
	    $isSecurePath = $_GET['security'];
	
    $isShared = false;
    if(isset($_GET['shared']))
	    $isShared = $_GET['shared'];

	if($path === "/")
		echo "<div><a style='text-decoration: none' href='".getPath("")."'>Return to Dropbox</a></div>";
	else
		echo "<div><a style='text-decoration: none' href='".getPath("?path=".htmlspecialchars($path).($isSecurePath? "&security=".true:"").($isShared? "&shared=".true:""))."'>Return to Dropbox</a></div>";
}

/**
 * Removes all of the "secure" files which are contained in the $folderPath. A boolean is
 * returned which is true if a file has been deleted and false if one has not.
 *
 * @param string $folderPath
 * 		path specifying the folder which all contained files are being removed from
 *
 * @return bool
 * 		true if a file has been deleted and false if one has not
 */
function removeSecureFilesInFolder($folderPath){
	$deleted = false;
	global $hss;
	$filesPaths = $hss->getFilesPaths();
	foreach ($filesPaths as $file){
		 if(isIncluded($folderPath, $file)){
			$hss->delete($file);
			$deleted = true;
    	}
    }
    return $deleted;
}

/**
 * Returns true if the $name is a valid folder name and false otherwise
 * 
 * @param string $name
 * 		the name of the folder in query
 * @param string $parent
 * 		the path which this folder is being created in
 *
 * @return bool
 * 		true if the $name is a valid folder name and false otherwise
 */
function isValidFolderName($name, $parent){
	$folderPath = ($parent === "/"? "":$parent)."/".$name;
	return !(strlen($folderPath) > 255 || strchr($name, '/') || strchr($name, '\\') || strchr($name, '<') || 
				strchr($name, '>') || strchr($name, ':') || strchr($name, '"') || 
				strchr($name, '|') || strchr($name, '?') || strchr($name, '*') || 
				strchr($name, '.'));
}

/**
 * Returns a string representing the folder or file in the current path, based on the whole path (i.e. to the actual file path).
 * Null is returned if the two parameters are the same.
 * 
 * @param string $currentPath
 * 		representing the current path that the file or folder following is being returned from. This is the path to the directory the
 * 		browser should be displaying folders and files for
 * @param string $wholePath 
 * 		representing the whole path which is being examined for the file or folder following the currentPath. This is the path to the
 * 		actual file that is "securely" stored.
 *
 * @return string
 * 		representing the folder or file name in the current path, based on the whole path. This includes
 * 		the '/' character at the end if it is a folder. Null is returned if the two parameters are the same.
 */
function getNextInPath($currentPath, $wholePath){
	if($currentPath == $wholePath)
		return NULL;
	$current = explode("/", $currentPath);
	$whole = explode("/", $wholePath);
	for($i = 0; $i < count($current); $i++){
		if($current[$i] == $whole[$i])
			unset($whole[$i]);
	}
	
	if($currentPath === "/"){
		return $whole[$i - 1].(!isFilePath($wholePath) || ($i - 1) != count($whole) ? "/":"");
	}else{
		return $whole[$i].(!isFilePath($wholePath) || ($i - 1) != count($whole) ? "/":"");
	}
}

/**
 * Returns a boolean which is true if the current path is in the whole path, 
 * if not false is returned. False is also returned if the two parameters are the same
 * 
 * @param string $currentPath
 * 		representing the current path which is being looked for completely in the $wholePath
 * @param string $wholePath
 * 		representing the whole path which the $currentPath is being looked for inside 
 *
 * @return bool
 * 		which is true if the current path is in the whole path, if not false is returned.
 * 		False is also returned if the two parameters are the same
 */
 function isIncluded($currentPath, $wholePath){
	if($currentPath == $wholePath)
		return false;
    if($currentPath === "/") return true;
 	$current = explode("/", $currentPath);
	$whole = explode("/", $wholePath);
	
	for($i = 0; $i < count($current); $i++){
		if($current[$i] != $whole[$i])
			return false;
	}
	
	return true;
 }
 
 /**
  * Prints to the screen that the file name has been taken and has a link to return to the previous
  * directory in the file browser
  * 
  * @param string $path
  * 	path to the file that was being uploaded, which is used to return to the parent directory
  */
function fileNameTakenError($path){
	echo "Error: File name already exists.<br>";
	echoReturnToParentLink($path);
}

/**
 * Renders a normal Dropbox file, printing its Metadata to the screen and allowing the user to download it
 * 
 * @param Metadata $entry
 * 		Dropbox metadata for the file to render
 */
function renderNormalFile($entry)
{
    $metadataStr = htmlspecialchars(print_r($entry, true));
    $downloadPath = getPath("download?path=".htmlspecialchars($entry['path']));
    $deletePath = getPath("delete?path=".htmlspecialchars($entry['path']));
    $body = <<<HTML
        <pre>$metadataStr</pre>
        _____________________________________________________________________________________________<br>
        _____<a href="$downloadPath">Download this file</a>_______________________________________________________<a href="$deletePath" onclick="return confirm('Are you sure you want to delete this file?');">Delete this file</a>______
        
HTML;

    return renderHtmlPage("File: ".$entry['path'], $body);
}

/**
 * Renders a "secure" file, printing its Metadata to the screen and allowing the user to download it. If the
 * user is the owner of the file, they are also given the option to delete the file and change its sharing settings
 * 
 * @param string $path 
 * 		representing the path to the "secure" file in the Dropbox library
 * @param bool $isShared
 * 		true if the file is shared i.e. not owned by this user, false otherwise.
 */
function renderSecureFile($path, $isShared)
{
	global $hss;
    $metadataStr = $hss->getMetadata($path, $isShared);
    $downloadPath = $hss->getFileDownloadLink($path, $isShared);
    
    if(!$isShared){
    	$sharePath = getPath("share?path=".htmlspecialchars($path)."&security=".true);
    	$deletePath = getPath("delete?path=".htmlspecialchars($path)."&security=".true);
    	$body = <<<HTML
        	<pre>$metadataStr</pre>
	        _____________________________________________________________________________________________<br>
    	    _____<a href="$downloadPath">Download this file</a>_____________
        	<a href="$sharePath">Change sharing options for this file</a>_____________
	        <a href="$deletePath" onclick="return confirm('Are you sure you want to delete this file?');">Delete this file</a>______    
HTML;

    }else{
    	$body = <<<HTML
        	<pre>$metadataStr</pre>
        	_____________________________________________________________________________________________<br>
        	_____<a href="$downloadPath">Download this file</a>_________________________________________________________________________
HTML;
    }
    

    return renderHtmlPage("File: ".$path, $body);
}

 /**
 * Returns true if the $path is to a file (i.e. there is a '.' character before the first '/', from right
 * to left), false otherwise.
 * 
 * @param string $path
 * 		path to the file
 * 
 * @return bool 
 * 		true if the $path is to a file (i.e. there is a '.' character before the first '/', from right
 * 		to left), false otherwise.
 */
function isFilePath($path){
 	for($i = (strlen($path) - 1); $i >= 0; $i--){
 		if($path{$i} == '.') return true;
 		if($path{$i} == '/') return false;
 	}
 	return false;
 }
 
/**
 * Returns an array containing the AppInfo, client identifier and user locale
 * 
 * @return array 
 * 		containing the AppInfo, client identifier and user locale
 */
 function getAppConfig()
{
    global $appInfoFile;

    try {
        $appInfo = dbx\AppInfo::loadFromJsonFile($appInfoFile);
    }
    catch (dbx\AppInfoLoadException $ex) {
        throw new Exception("Unable to load \"$appInfoFile\": " . $ex->getMessage());
    }

    $clientIdentifier = "examples-web-file-browser";
    $userLocale = null;

    return array($appInfo, $clientIdentifier, $userLocale);
}

/**
 * Returns the Dropbox Client which is logged in
 * 
 * @param Client
 * 		the Dropbox Client which is logged in
 */
function getClient()
{
    if (!isset($_SESSION['access-token'])) {
        return false;
    }

    list($appInfo, $clientIdentifier, $userLocale) = getAppConfig();
    $accessToken = $_SESSION['access-token'];
    return new dbx\Client($accessToken, $clientIdentifier, $userLocale, $appInfo->getHost());
}

/**
 * Returns the WebAuth for this App
 * 
 * @return WebAuth 
 * 		for this App
 */
function getWebAuth()
{
    list($appInfo, $clientIdentifier, $userLocale) = getAppConfig();
    $redirectUri = getUrl("dropbox-auth-finish");
    $csrfTokenStore = new dbx\ArrayEntryStore($_SESSION, 'dropbox-auth-csrf-token');
    return new dbx\WebAuth($appInfo, $clientIdentifier, $redirectUri, $csrfTokenStore, $userLocale);
}

/**
 * Renders the HTML page with the given $title and $body
 * 
 * @param string $title
 * 		the title of the HTML page
 * @param string $body
 * 		the body of text for the HTML page
 */
function renderHtmlPage($title, $body)
{
    return <<<HTML
    <html>
        <head>
            <title>$title</title>
        </head>
        <body>
            <h1>$title</h1>
            $body
        </body>
    </html>
HTML;
}

/**
 * Responds to an error by redirecting the browser and rendering an error message
 * 
 * @param int $code
 * 		code given as an ID for the error message
 * @param string $title
 * 		title to be displayed on the HTML error message
 * @param string $body
 * 		body of text to be displayed with the HTML error message. Default is blank.
 */
function respondWithError($code, $title, $body = "")
{
    $proto = $_SERVER['SERVER_PROTOCOL'];
    header("$proto $code $title", true, $code);
    echo renderHtmlPage($title, $body);
}

/**
 * Returns the URL based on the $relative_path
 * 
 * @param string $relative_path
 * 		relative path to base the URL on
 * 
 * @return string
 * 		URL based on the relative path
 */
function getUrl($relative_path)
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
        $scheme = "https";
    } else {
        $scheme = "http";
    }
    $host = $_SERVER['HTTP_HOST'];
    $path = getPath($relative_path);
    return $scheme."://".$host.$path;
}

/**
 * Returns the path based on the $relative_path. This is used to redirect pages.
 *
 * @param string $relative_path
 * 		the relative path to base the path returned on
 *
 * @return string
 * 		the path based on the $relative_path
 */
function getPath($relative_path)
{
    if (PHP_SAPI === 'cli-server') {
        return "/".$relative_path;
    } else {
        return $_SERVER["SCRIPT_NAME"]."/".$relative_path;
    }
}

/**
 * Initialisation method
 * 
 * @return string
 * 		path/script name based on the server this file is being launched in
 */
function init()
{
    global $argv;

    // If we were run as a command-line script, launch the PHP built-in web server.
    if (PHP_SAPI === 'cli') {
        launchBuiltInWebServer($argv);
        assert(false);
    }

    if (PHP_SAPI === 'cli-server') {
        // For when we're running under PHP's built-in web server, do the routing here.
        return $_SERVER['SCRIPT_NAME'];
    }
    else {
        // For when we're running under CGI or mod_php.
        if (isset($_SERVER['PATH_INFO'])) {
            return $_SERVER['PATH_INFO'];
        } else {
            return "/";
        }
    }
}

/**
 * Lanches a built in web server used for running the web-file-browser as a script. NOTE: This has never been tested for
 * my App.
 * 
 * @param array $argv
 * 		arguements for the built in web server
 */
function launchBuiltInWebServer($argv)
{
    // The built-in web server is only available in PHP 5.4+.
    if (version_compare(PHP_VERSION, '5.4.0', '<')) {
        fprintf(STDERR,
            "Unable to run example.  The version of PHP you used to run this script (".PHP_VERSION.")\n".
            "doesn't have a built-in web server.  You need PHP 5.4 or newer.\n".
            "\n".
            "You can still run this example if you have a web server that supports PHP 5.3.\n".
            "Copy the Dropbox PHP SDK into your web server's document path and access it there.\n");
        exit(2);
    }

    $php_file = $argv[0];
    if (count($argv) === 1) {
        $port = 5000;
    } else if (count($argv) === 2) {
        $port = intval($argv[1]);
    } else {
        fprintf(STDERR,
            "Too many arguments.\n".
            "Usage: php $argv[0] [server-port]\n");
        exit(1);
    }

    $host = "localhost:$port";
    $cmd = escapeshellarg(PHP_BINARY)." -S ".$host." ".escapeshellarg($php_file);
    $descriptors = array(
        0 => array("pipe", "r"),  // Process' stdin.  We'll just close this right away.
        1 => STDOUT,              // Relay process' stdout to ours.
        2 => STDERR,              // Relay process' stderr to ours.
    );
    $proc = proc_open($cmd, $descriptors, $pipes);
    if ($proc === false) {
        fprintf(STDERR,
            "Unable to launch PHP's built-in web server.  Used command:\n".
            "   $cmd\n");
        exit(2);
    }
    fclose($pipes[0]);  // Close the process' stdin.
    $exitCode = proc_close($proc);  // Wait for process to exit.
    exit($exitCode);
}

?>