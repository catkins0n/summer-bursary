<?php

require_once __DIR__.'/../../lib/Google/autoload.php';
include_once "templates/base.php";

/**
 * Redirects the Client's browser to authenticate their Google Drive with this App.
 * After authentication has occurred the browser is redirected to the web-file-browser.php 
 * (in the current directory)
 * 
 * IMPORTANT: For this to work the $client_id & $secrete_key must be set to specify an App created 
 * in the Developer's Console. Furthermore the redirect uri must be added to the App specified 
 * in the Developer's Console, so that the redirect path for this file is specified. 
 * Developers Console: https://console.developers.google.com
 * 
 * @author Craig Atkinson
 * @date 05/12/2014
 */

session_start();

$client_id = '<App_key>';
$client_secret = '<App_secret>';
$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setScopes(array(
//	'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
    ));

//////////////////////////////////////////////////////////////
///////////uncomment the following line to logout ////////////
/////////////////then recomment and run again/////////////////
//////////////////////////////////////////////////////////////
//unset($_SESSION['access_token']);

if (isset($_GET['code'])) {
  	$client->authenticate($_GET['code']);
 	$_SESSION['access_token'] = $client->getAccessToken();
  	$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if ($client->getAccessToken()) {
	$_SESSION['access_token'] = $client->getAccessToken();
  	$token_data = $client->verifyIdToken()->getAttributes();
}

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	header("Location: " . 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/web-file-browser.php");
}else{
	echo "<header><h1>" . "Google: Logged out/Access token expired" . "</h1></header>";
	$authUrl = $client->createAuthUrl();
	echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
  	echo " to log in and try again.<br><br>";
  	echo "To switch accounts please visit: <b>https://accounts.google.com/Login?hl=EN</b><br>
  			and then ";
  	echo "<a class='login' href='" . $authUrl . "'>Click Here</a>";
}

if (
		$client_id == '<YOUR_CLIENT_ID>'
    	|| $client_secret == '<YOUR_CLIENT_SECRET>'
    	|| $redirect_uri == '<YOUR_REDIRECT_URI>'
    ) {
  	echo missingClientSecretsWarning();
}
?>