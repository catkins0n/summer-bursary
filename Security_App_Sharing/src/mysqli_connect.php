<?php

/**
 * Provides a function which connects to the MySQL Database: secureDatabaseInfo, returning the open connection
 * which can be used for querys.
 *
 * @author Craig Atkinson
 * @date 13/02/2015
 */
DEFINE ('DB_USER', 'Client');
DEFINE ('DB_PASSWORD', 'catkins0n');
DEFINE ('DB_HOST', 'localhost');
DEFINE ('DB_NAME', 'secureDatabaseInfo');

/**
 * Returns an open database connection with the MYSQL Database: secureDatabaseInfo. This connection can be used
 * for making querys
 * 
 * @return object
 * 		an open database connection with the MYSQL Database: secureDatabaseInfo. This connection can be used
 * 		for making querys
 */
function getDatabaseConnection(){
	$dbc = @mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
	OR die('Could not connect to MySQL: ' .
	mysqli_connect_error());
	return $dbc;
}

?>

