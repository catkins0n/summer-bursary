#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
rm -rf /var/www
ln -fs /vagrant /var/www
apt-get -y install emacs
apt-get -y install libapache2-mod-php5
a2enmod php5
apt-get install -y curl libcurl3 libcurl3-dev php5-curl
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql
cp /vagrant/php.ini /etc/php5/apache2/php.ini

echo "DROP DATABASE IF EXISTS secureDatabaseInfo" | mysql -uroot -proot
echo "CREATE DATABASE secureDatabaseInfo" | mysql -uroot -proot
echo "GRANT INSERT, SELECT, DELETE, UPDATE ON secureDatabaseInfo.* TO 'Client'@'localhost' IDENTIFIED BY 'catkins0n'" | mysql -uroot -proot
echo "CREATE TABLE secureDatabaseInfo.accountFiles(googleID VARCHAR(21) NOT NULL, fileID VARCHAR(64) NOT NULL, PRIMARY KEY(googleID, fileID))" | mysql -uroot -proot
echo "CREATE TABLE secureDatabaseInfo.filesPaths(fileID VARCHAR(64) NOT NULL PRIMARY KEY, path VARCHAR(255) NOT NULL)" | mysql -uroot -proot
echo "CREATE TABLE secureDatabaseInfo.sharedAccountFiles(email VARCHAR(255) NOT NULL, fileID VARCHAR(64) NOT NULL, permission VARCHAR(6) NOT NULL, PRIMARY KEY(email, fileID))" | mysql -uroot -proot
echo "CREATE TABLE secureDatabaseInfo.sharedFilesPaths(fileID VARCHAR(64) NOT NULL PRIMARY KEY, path VARCHAR(255) NOT NULL)" | mysql -uroot -proot

service apache2 restart